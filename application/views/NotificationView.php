<br>
<div class="row">
    <div class="col-md-5">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" name="search_query" id="search_query" class="form-control"
                       placeholder="Cari Pesan ..."/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
            </div>
        </div>
    </div>
</div>

<div id="modal_detail" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="height: auto;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <br>
                <p style="font-size: 18px;color: #757575;font-weight: bold;">Customer Name : <a style="color: #2860ff"
                                                                                                id="nama_anggota"></a>
                </p>

                <input type="text" class="hidden" id="user_id" name="user_id">

                <p>Status : <a style="font-size: 12px;font-weight: bold" id="status"></a>
                    <button id="btnReactive" type="button" class="btn btn-success" onclick="reactive()"
                            style="display: none">Re-active
                    </button>
                </p>
                <p id="member_sejak" style="font-size: 12px;color: #757575"></p>
                <p id="address" style="font-size: 12px;color: #757575"></p>
                <p id="phone_number" style="font-size: 12px;color: #757575"></p>
                <p id="email" style="font-size: 12px;color: #757575"></p>
                <p id="note" style="font-size: 12px;color: #757575"></p>
                <p id="member_sejak" style="font-size: 12px;color: #757575"></p>

                <br>
                <p id="member_sejak" style="font-size: 14px;color: #4f4f4f;font-weight: bold;">EMERGENCY CONTACT</p>

                <p id="emergency_contact" style="font-size: 12px;color: #757575"></p>
                <p id="emergency_phone_number" style="font-size: 12px;color: #757575"></p>
                <p id="note" style="font-size: 12px;color: #757575"></p>

                <br>
                <p id="member_sejak" style="font-size: 14px;color: #4f4f4f;font-weight: bold;">MEMBERSHIP
                    INFORMATION</p>

                <p id="member_type" style="font-size: 12px;color: #757575"></p>
                <p id="total_quota" style="font-size: 12px;color: #757575"></p>
                <p id="day_quota" style="font-size: 12px;color: #757575"></p>

                <br>
                <button type="button" class="btn btn-primary" onclick="doEdit()">EDIT</button>
            </div>
        </div>
    </div>
</div>

<br>
<div id="response">
    <table id="table-notification" class="table table-striped">
        <thead>
        <tr style="background-color: #f2f2f2">
            <th>User</th>
            <th>Pesan</th>
            <th>Tanggal</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<input type="hidden" id="pageno" value="0">
<img id="loader" src="<?= base_url() ?>/assets/img/loader.svg">

<script>
    $(document).ready(function () {
        searchData();

        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                searchData();
            }
        });


        $('#search_query').keypress(function (e) {
            if (e.which == 13) {
                resetAndSearchData();
            }
        });

    });

    function resetAndSearchData() {
        $('#pageno').val("0");
        $("#table-notification").find("tr:gt(0)").remove();
        searchData();
    }

    function searchData() {
        var nextPage = parseInt($('#pageno').val()) + 1;
        $.ajax({
            type: 'GET',
            url: '<?= $cUri ?>/getAll',
            data: {
                pageno: nextPage, search_query: $('#search_query').val()
            },
            success: function (result) {
                if (result.data.length > 0) {

                    $i = 0;
                    $.each(result.data, function () {
                        var color = ($i % 2 == 0) ? '' : 'active';

                        $html = '<tr><td>' + this.nama_lengkap + '</td><td>' + this.message + '</td><td>' + this.created_date + '</td></tr>';

                        $('#table-notification tr:last').after($html);
                        $('#pageno').val(nextPage);

                        $i++;
                    });
                }

                $("#loader").hide();
            }
        });
    }
</script>