<br>
<div class="row">
    <div class="col-md-8">
        <p style="color: #063a69;font-size: 16px;font-weight: bold;">DASHBOARD</p>
        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #367bf5, #36c7f5)">
                    <div style="font-size: 14px;font-weight: bold">Total Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalSurat ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i
                            style="position: absolute; right: 15px; bottom: 15px" class="fa fa-arrow-down"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #ed5454, #f8a406)">
                    <div style="font-size: 14px;font-weight: bold">Disposisi Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalAssigned ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i
                            style="position: absolute; right: 15px; bottom: 15px" class="fa fa-arrow-up"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #2fa84f, #85e259)">
                    <div style="font-size: 14px;font-weight: bold">Approve Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalApproved ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i
                            style="position: absolute; right: 15px; bottom: 15px" class="fa fa-check"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #ed5454, #FD4744)">
                    <div style="font-size: 14px;font-weight: bold">Pendingan Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalRequested ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i
                            style="position: absolute; right: 15px; bottom: 15px" class="fa fa-hourglass-end"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">

        <div class="row">
            <div class="col-md-8">
                <p style="color: #063a69;font-size: 16px;font-weight: bold;">SURAT MASUK HARI INI</p>
            </div>
            <div class="col-md-4">
                <?= date('d M Y') ?>
            </div>
        </div>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col"></th>
                <th scope="col">Requester</th>
                <th scope="col">Surat</th>
            </tr>
            </thead>
            <tbody>

            <?php
            $i = 1;
            foreach ($todaySuratList as $surat) {
                ?>
                <tr>
                    <th scope="row"><?= $i ?></th>

                    <?
                    if ($surat->status == 'REQUESTED') {
                        echo '<td><span class="label label-default">' . $surat->status . '</td>';
                    } else if ($surat->status == 'APPROVED') {
                        echo '<td><span class="label label-success">' . $surat->status . '</td>';
                    } else if ($surat->status == 'ASSIGNED') {
                        echo '<td><span class="label label-warning">' . $surat->status . '</td>';
                    } else if ($surat->status == 'REJECTED') {
                        echo '<td><span class="label label-danger">' . $surat->status . '</td>';
                    }
                    ?>
                    <td><?= $surat->nama_lengkap ?></td>
                    <td><?= $surat->no_surat ?></td>
                </tr>
                <?
            }
            ?>

            <tr>
                <td scope="col" colspan="4" style="text-align: right">
                    <a class="link-blue" href="<?= base_url('requestsurat/admin') ?>">View All</a>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</div>


<br>
<br>
<div class="row">
    <div class="col-md-8">
        <p style="color: #063a69;font-size: 16px;font-weight: bold;">POSISI SURAT</p>

        <table id="table-summary" class="table table-striped">
            <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th style="color: #2fa84f" scope="col">Surat Approved</th>
                <th style="color: #ed5454" scope="col">Surat Rejected</th>
                <th style="color: #f8a406" scope="col">Surat Pending</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="col-md-4">

    </div>

</div>


<div id="response">
</div>

<script>
    getTotalSummaryRequest();

    function getTotalSummaryRequest() {
        $.ajax({
            type: 'GET',
            url: '<?= $cUri ?>/getTotalSummaryRequest',
            success: function (result) {

                $i = 1;

                $.each(result, function () {
                    $html = '<tr>' +
                        '<td>' + $i + '</td>' +
                        '<td>' + this.fullname + '</td>' +
                        '<td>' + this.totalApproved + '</td>' +
                        '<td>' + this.totalRejected + '</td>' +
                        '<td>' + this.totalPending + '</td>' +
                        '</tr>';

                    $('#table-summary tr:last').after($html);
                    $i++;
                });

            }
        });
    }

</script>