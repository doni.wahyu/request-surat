<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller
{
    var $title = "Notification";
    var $cUri = "notification";
    var $menuName = "Notification";

    function __construct()
    {
        parent::__construct();
        $this->load->model('NotificationModel');
    }

    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'NOTIFICATION LIST';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $notification = new NotificationModel();
        $dataTable = $notification->dbGetAll();
        $data['dataTable'] = $dataTable;

        $this->load->view('HeaderView', $data);
        $this->load->view('NotificationView', $data);
        $this->load->view('FooterView', $data);
    }

    public function getAll()
    {
        $pageno = $_GET['pageno'];
        $search_query = $_GET['search_query'];

        $no_of_records_per_page = 10;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $notification = new NotificationModel();

        $query = "SELECT notification.*, user.nama_lengkap FROM notification
                  inner join user on notification.created_by = user.user_id
                  where 1 = 1 and notification.user_id = " . getSessionUserId();

        if ($search_query != '') {
            $query .= " AND (message LIKE '%$search_query%' or nama_lengkap LIKE '%$search_query%')";
        }

        $query .= " LIMIT $offset, $no_of_records_per_page";

        $data = $notification->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }
}