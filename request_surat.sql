/*
SQLyog Ultimate v9.50 
MySQL - 5.1.41-log : Database - request_surat
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `attachment` */

CREATE TABLE `attachment` (
  `attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_surat_id` int(11) DEFAULT NULL,
  `file_attachment` text,
  `created_by` varchar(100) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attachment` */

/*Table structure for table `jenis_surat` */

CREATE TABLE `jenis_surat` (
  `jenis_surat_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_name` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jenis_surat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `jenis_surat` */

insert  into `jenis_surat`(`jenis_surat_id`,`jenis_name`,`created_by`,`created_date`,`updated_by`,`updated_date`) values (1,'KPA',NULL,'2020-02-28 10:43:56',NULL,NULL),(2,'DIRJEN',NULL,'2020-02-28 10:43:59',NULL,NULL),(3,'MENTERI DALAM NEGERI',NULL,'2020-02-28 10:44:10',NULL,NULL);

/*Table structure for table `notification` */

CREATE TABLE `notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_surat_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` text,
  `is_viewed` int(1) NOT NULL DEFAULT '0',
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notification` */

/*Table structure for table `request_surat` */

CREATE TABLE `request_surat` (
  `request_surat_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_surat_id` int(11) DEFAULT NULL,
  `no_surat` varchar(150) DEFAULT NULL,
  `judul_surat` text,
  `nota_dinas` text,
  `tanggal_surat` date DEFAULT NULL,
  `revision_no` int(11) DEFAULT NULL,
  `status` enum('REQUESTED','APPROVED','REJECTED','ASSIGNED') DEFAULT NULL,
  `pdf_file` varchar(50) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`request_surat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `request_surat` */

/*Table structure for table `request_surat_assign` */

CREATE TABLE `request_surat_assign` (
  `request_surat_assign_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_surat_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `assign_date` datetime DEFAULT NULL,
  `followup_date` datetime DEFAULT NULL,
  `point` int(11) DEFAULT NULL COMMENT '''follow up = 2, skip = -1''',
  `created_by` varchar(100) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`request_surat_assign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `request_surat_assign` */

/*Table structure for table `request_surat_detail` */

CREATE TABLE `request_surat_detail` (
  `request_surat_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_surat_id` int(11) DEFAULT NULL,
  `type_detail` enum('menimbang','mengingat','memperhatikan','memutuskan') DEFAULT NULL,
  `isi` text,
  `revision_no` int(11) DEFAULT NULL,
  `comment` text,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`request_surat_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `request_surat_detail` */

/*Table structure for table `user` */

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_hp` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `spv_id` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `role` enum('ADMIN','REQUESTER','STAFF','SUPER ADMIN') DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`nip`,`nama_lengkap`,`email`,`password`,`no_hp`,`jabatan`,`spv_id`,`is_active`,`role`,`profile_pic`,`created_by`,`created_date`,`updated_by`,`updated_date`) values (1,'001','admin','admin','21232f297a57a5a743894a0e4a801fc3','','Administrator',NULL,1,'ADMIN','51fd91b94de89fcf3918a25a6ad37bf8.jpg',NULL,'2019-05-19 17:41:21','1','2019-06-26 11:56:06'),(6,'006','Staff 1','staff1','5f4dcc3b5aa765d61d8327deb882cf99',NULL,'Staff',3,1,'STAFF',NULL,'1','2019-06-02 16:11:29','1','2019-06-14 16:21:48'),(7,'007','Staff 2','staff2','5f4dcc3b5aa765d61d8327deb882cf99',NULL,'Staff',3,1,'STAFF',NULL,'1','2019-06-02 16:13:09','1','2019-06-02 16:15:04'),(8,'192091','Staff 3','staff3','5f4dcc3b5aa765d61d8327deb882cf99',NULL,'Staff',5,1,'STAFF',NULL,'1','2019-06-14 15:22:15','1','2019-06-14 15:23:47'),(10,'787878','Requester 1','requester1','5f4dcc3b5aa765d61d8327deb882cf99',NULL,'Requester',2,1,'REQUESTER',NULL,'1','2019-06-14 15:38:47','1','2019-06-14 15:52:02'),(11,'002','kabag','kabag','5f4dcc3b5aa765d61d8327deb882cf99',NULL,'Kabag',NULL,1,'SUPER ADMIN',NULL,NULL,'2020-07-05 17:17:18',NULL,NULL);

/*Table structure for table `user_activity` */

CREATE TABLE `user_activity` (
  `user_activity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `device_info` text,
  `keycode` varchar(255) DEFAULT NULL,
  `firebase_keycode` varchar(255) DEFAULT NULL,
  `login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

/*Data for the table `user_activity` */

insert  into `user_activity`(`user_activity_id`,`user_id`,`device_info`,`keycode`,`firebase_keycode`,`login_date`,`logout_date`) values (1,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36','S4c22h5ERvMd3C98K3B8Z63Oz90G2eZwjXz56rKAW06FDgv5ce',NULL,'2020-03-16 07:58:56',NULL),(2,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36','rR4IRnf4yo8b73tS0tpMuzaPev7E89Y0rNJ3dg9dF9acfjfcNe',NULL,'2020-03-16 07:59:54','2020-03-16 08:00:19'),(3,6,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36','97TWA573fjahLaa6dS3e96W9QBaa9bDcL1Bc7z7bTfe8f16dUa',NULL,'2020-03-16 08:00:27',NULL),(4,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0','3e5IJIK1mellDaBtoMkdz820dl7zTgsIvy2baFW6U8s280vakP',NULL,'2020-05-06 09:25:09','2020-05-06 09:26:07'),(5,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0','7110KpS3XC799bXFfDo6braC8cbc39tUNgy2Geb7yS1sJZ1p70',NULL,'2020-05-06 11:27:50',NULL),(6,1,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','QekAaT70Z6ca7bFqcXh5tDfAr11AGfa1ebB7477ydxwkJaLfD7',NULL,'2020-06-04 10:48:04',NULL),(7,1,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','yas4i694vi61fctWN1l4TocFhLeJbauK0WJ3qF3VX8DvY69fJg',NULL,'2020-06-04 11:05:02','2020-06-04 11:05:11'),(8,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','Gnafzed0akXapZfeea52aemdQKNaadMkfxeOs7P6rhdR57F84w',NULL,'2020-06-04 11:05:23',NULL),(9,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','1tG5cb686mf98Pd580b8P12P5f58PiEQM8oY1fNYBqtj4n05nc',NULL,'2020-06-04 11:05:23','2020-06-04 11:05:37'),(10,10,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','faX9bbXXfPeb8M5o7cafE0607fGMe2fffN0QZXiPhxbf98ec9f',NULL,'2020-06-04 11:06:02',NULL),(11,10,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','VbAmdgde7378887mk3a0T0a2bfR0oBJaN0a847Md9y8JfffzFb',NULL,'2020-06-04 11:06:02',NULL),(12,10,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','bbaE2467GU6v41R3ysI0PYf06mHf2b36bfv1YB880fE4554xse',NULL,'2020-06-04 11:06:03',NULL),(13,10,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','3Hafk3a1s41ybJqaPqcUhi1MiJawYs3O3cG71i0c31LaLbD46r',NULL,'2020-06-04 11:48:06','2020-06-04 11:48:39'),(14,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','21bCW2YxxVisZn0Yd212oSeeb1bqvUXyPe1f9DNc36F55fve59',NULL,'2020-06-04 11:49:04',NULL),(15,1,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','79cSM0UW6StJLCiifXfGUtm6fhyzf0JY3dkQ7cheBKXccc97Hp',NULL,'2020-06-04 16:05:12','2020-06-04 16:05:51'),(16,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','282826J34fe4C25776U2T547dw2M4YVaJS7Gpjbl8p4Ls2Sz4h',NULL,'2020-06-04 16:05:59','2020-06-04 16:06:09'),(17,10,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','lWa6H7DNe922feWuIISlkcHtbVkhAGvV6fsN3147c2TWdk7j6O',NULL,'2020-06-04 16:06:18','2020-06-04 16:08:40'),(18,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','zf3GsAo8ffEaB133qf5L2mQKPP7bupq5Eued3CQ3u2a50QRqIO',NULL,'2020-06-04 16:08:54','2020-06-04 16:09:08'),(19,1,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','590l2LF70fsM0IaJMONByq800iakfIZ1cP2b5H00xiIs9q4g5t',NULL,'2020-06-04 16:09:13','2020-06-04 16:09:39'),(20,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','w4463pRc6jAPbaaomTz6aU3HCtPQf4w3arcYQpPXJ4hu4rTr74',NULL,'2020-06-04 16:09:46','2020-06-04 16:10:30'),(21,1,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','e0To6ct0rP4bkddNEec29dz0cY0b3aeiU6GxF0a4t6bNV644Zc',NULL,'2020-06-04 16:10:40',NULL),(22,10,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0','ST14nKbSbDedg9H4dNFcfOlmCf581kYTfZK4eWX53dikbZo8g7',NULL,'2020-06-08 14:08:47','2020-06-08 14:18:55'),(23,6,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0','I1afL4di8QYDd3vQdTbw4enchct6Fi165J7GplZx4s2QwaaJad',NULL,'2020-06-08 14:09:36','2020-06-08 14:13:08'),(24,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0','x6fZO7Wo7wn67D0HN195reieTv15Vdp74eXwcnUaTc5rp3580c',NULL,'2020-06-08 14:13:11','2020-06-08 14:13:43'),(25,6,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0','rbudHfGRw6Jp5I91SB2O1JSMRQfc8r5A6hoH4b2r7MR06mfRXY',NULL,'2020-06-08 14:13:49','2020-06-08 14:16:25'),(26,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0','cf7Z14bbEgcvbiDSHZl0GOU0ecbyCdifep00o6ccdG27ZGZa95',NULL,'2020-06-08 14:16:29',NULL),(27,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36','MncaQi3pwOegUm3GjHS9jDo2DvE4sRd331Mz6GZDa3UT5S4p4W',NULL,'2020-06-08 14:41:23','2020-06-08 14:42:34'),(28,6,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36','HYm17R70q74RK15K69Lf4W0M7IeaA813wg6Db0x3dfUuaQ9dPu',NULL,'2020-06-08 14:42:41','2020-06-08 14:43:11'),(29,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36','PDNQsm87YX4eEcXmAD76zF9G8PajY3ciD7deaFzP6myLy32dCb',NULL,'2020-06-08 14:43:18',NULL),(30,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36','3103J6MIopKEe0866m1sBRf65efz8o1c3ffMM0618Ii1yr8wO5',NULL,'2020-06-16 09:55:54','2020-06-16 09:56:00'),(31,10,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36','3XHayb62AfPM0s1cSRxuqqzEccCcJe1vxIF1GqJ3q3wfv6caYs',NULL,'2020-06-16 09:56:15','2020-06-16 09:57:00'),(32,6,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36','4fbkodc5ocdYfc8bQ0fbjLefGc0x4A3fdU5BL3A5UqCA4rwN85',NULL,'2020-06-16 09:57:08','2020-06-16 09:57:38'),(33,10,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36','b4lb0gfjka3aPc33f0Af67CecJTys18oRfjF9yZttR3er02j4n',NULL,'2020-06-16 09:58:32',NULL),(34,1,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','a6641pr9R84aZ3397Cbb3tbSU0F374gT7frKlST32zXu87Ed6g',NULL,'2020-06-16 10:21:10','2020-06-16 10:21:21'),(35,10,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','ef45691kiB6f05ea9TN7e3Kihh8BPw5lLbryfsT89Q63Vc9u3y',NULL,'2020-06-16 10:21:40','2020-06-16 10:21:46'),(36,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','eXBlASAK54Mv20hqHtn8A8W6Uec6010XZC23eoO4i5Al5SLN73',NULL,'2020-06-16 10:21:52','2020-06-16 10:23:39'),(37,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','p75jFbaLZ5MrVaeYdDheVmxfsav558o8atsycmc7hA77umFmZX',NULL,'2020-06-16 10:46:28','2020-06-16 10:47:06'),(38,10,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','w68hXI42p31ac6z6F6dMJGFBo56eaVAGzIY0e22l2fk0FUE5rR',NULL,'2020-06-16 10:47:13','2020-06-16 10:56:45'),(39,6,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36','cSUx8p0TcJU777NRN05y9888D62vEuRx2g8WFJjhc0p232zDVE',NULL,'2020-06-16 10:56:55',NULL),(40,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','Khm5VL6rXBdqNGnrddmgs05oX2nB04k444c0IEf6Um72x9DDjb',NULL,'2020-07-04 18:25:53','2020-07-04 18:26:16'),(41,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','Ud12cQ9cefx9JYcb6XoR70E0l2kCm1f20U25234gdmsc52CiBY',NULL,'2020-07-04 18:26:25',NULL),(42,6,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','MGPm2cvf2S5ML7uUaikH58w24477H5csY8JBG5F8jK868P2kIS',NULL,'2020-07-04 18:29:26',NULL),(43,6,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','fCS0SJW73D0S8SbnN20G6zaqdkBvzYw5UK86naRrOwrhgPO4BP',NULL,'2020-07-04 18:29:26','2020-07-04 18:29:41'),(44,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','3Jz3aO5EenHw88JX9NVXb36gl7RVrLv95Z76fW2o00c4Xcct05',NULL,'2020-07-04 18:29:47','2020-07-04 18:30:21'),(45,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','w3511n2Ee5bgf4iZI687H0dhJiGgx4V3F64iTdwfbFWc44a9o4',NULL,'2020-07-04 18:30:30','2020-07-04 18:32:15'),(46,6,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','5gOXcjb5ZDY1wek5EnVcW5qKcCi957ffN7esvdWSv8lpp2CIvD',NULL,'2020-07-04 18:32:20','2020-07-04 18:42:01'),(47,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','Ua260P4X3Jxb4z4P1k6KlGG306cHbN0DB3LJSvG0tcEt2XP1tb',NULL,'2020-07-04 18:42:04',NULL),(48,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','hCodbbM3b54bafcbbvcdCTig8Xnbw6cUlJfEclkcsYrhg8mP32',NULL,'2020-07-05 15:57:00','2020-07-05 16:17:25'),(49,11,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','1QTTR6fh0T82fZFG82bsItt7cUUf8Kmx97Hx8f5LQj0YjFoao3',NULL,'2020-07-05 16:18:15','2020-07-05 18:20:51'),(50,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','6gl7RVrLv35Z76eW2o0036X33t823w35een2Ee42gf4iZI687H',NULL,'2020-07-05 16:19:46','2020-07-05 18:21:09'),(51,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','e8h3SdZaAeceuRwxbedl0uZbcL8DxguE8c80d0a6g59dab8TYB',NULL,'2020-07-05 18:21:40','2020-07-05 18:22:13'),(52,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','z07446GfaSC5321owS5Gfd52gf47i2y06fKu21fZqa25XQWV56',NULL,'2020-07-05 18:22:21','2020-07-05 18:23:35'),(53,6,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','adB3P0rwngrem1WXHI6GDqX141oZcd7k4925nfd1tNEEB7neKD',NULL,'2020-07-05 18:23:41','2020-07-05 18:24:22'),(54,11,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','M4YIz3w259VdE93a22266moGOF4b4AbTfQAia7oqd7G70OPq3o',NULL,'2020-07-05 18:24:31','2020-07-05 18:30:29'),(55,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','dhJiGgx2VdF65iTdwfbFWadca9o4014aZxVYzu0P02cbfRec4d',NULL,'2020-07-05 18:30:37','2020-07-05 18:31:00'),(56,1,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','cK3yv0x2btfFid4M3kYQV0290bnFvGSbfr4292dfyb5M2bi0o6',NULL,'2020-07-05 18:31:06','2020-07-05 18:31:39'),(57,7,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','b9J1ZCEDcd70LOBa1cxSliD8cdbKRbnc7m2Kp6X2WbMv8cu3ez',NULL,'2020-07-05 18:31:45','2020-07-05 18:32:45'),(58,10,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','144ZxVYzu2Pddc4bR8c4a21bJ1ZCEDade0LOBad9xSliDead1K',NULL,'2020-07-05 18:32:51',NULL),(59,7,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','k2OJnRr02nu5Y6Ip2d63eK4v93Nu7UR4o4G8kc0rv4S323122o',NULL,'2020-07-05 18:33:47','2020-07-05 18:34:11'),(60,11,'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36','iat8fNSVzZP8epe8q5DceA2cXt90ZV3Ii2bWa8R805GG5Dy75W',NULL,'2020-07-05 18:34:16','2020-07-05 18:35:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
