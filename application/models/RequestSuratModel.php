<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class RequestSuratModel extends BaseModel
{
    function RequestSuratModel()
    {
        parent::__construct();
        $this->tableName = 'request_surat';
        $this->primaryKeyName = 'request_surat_id';
    }

    function getAll($where, $sort, $order, $limit, $page)
    {
        $start = $limit * $page - $limit;
        if ($start < 0) $start = 0;

        $this->db->select('request_surat.*, jenis_name');
        $this->db->from('request_surat');
        $this->db->join('jenis_surat', 'material.request_surat_id = jenis_surat.jenis_surat_id', 'left');


        if (count($where) > 0) {
            foreach ($where as $filter) {
                foreach ($filter as $key => $value) {
                    $this->db->where($key, $value);
                }
            }
        }

        $this->db->order_by($sort, $order);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }


    function getTotalRequest($user_id, $status)
    {
        $sql = "select count(*) as total from request_surat
                inner join (
                select request_surat_assign.*
                from request_surat_assign inner join (
                select max(request_surat_assign_id) as request_surat_assign_id, request_surat_id from request_surat_assign
                group by request_surat_id) a
                on request_surat_assign.request_surat_assign_id = a.request_surat_assign_id
                ) last_assign
                on request_surat.request_surat_id = last_assign.request_surat_id
                where last_assign.user_id = $user_id";

        if ($status == 'APPROVED' || $status == 'REJECTED') {
            $sql .= " and status = '$status'";
        } else if ($status == 'PENDING') {
            $sql .= " and status not IN ('APPROVED', 'REJECTED')";
        }

        $res = $this->db->query($sql)->result();
        return $res[0]->total;
    }


}