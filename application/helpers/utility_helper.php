<?php

define('UPLOAD_DATA_DIR', 'data/');
define('INCREASE_POINT', 2);
define('DECREASE_POINT', -1);

if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists("now")) {
    function now()
    {
        return date('Y-m-d H:i:s');
    }
}

if (!function_exists("nowDate")) {
    function nowDate()
    {
        return date('Y-m-d');
    }
}

if (!function_exists("nowTime")) {
    function nowTime()
    {
        return date('H:i:s');
    }
}


if (!function_exists("printJson")) {
    function printJson($controller, $res)
    {
        return $controller->output->set_content_type('application/json')->set_output(json_encode($res));
    }
}


if (!function_exists("uppercaseTrim")) {
    function uppercaseTrim($source)
    {
        return strtoupper(trim($source));
    }
}


function safe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
    return $data;
}

function safe_b64decode($string)
{
    $data = str_replace(array('-', '_'), array('+', '/'), $string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}

function encryptEncode($value)
{
    if (!$value) {
        return false;
    }
    $text = $value;
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SKEY, $text, MCRYPT_MODE_ECB, $iv);
    return trim(safe_b64encode($crypttext));
}

function encryptDecode($value)
{
    if (!$value) {
        return false;
    }
    $crypttext = safe_b64decode($value);
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SKEY, $crypttext, MCRYPT_MODE_ECB, $iv);
    return trim($decrypttext);
}

if (!function_exists("getDayOfWeek")) {
    function getDayOfWeek($booking_date)
    {
        return date('w', strtotime($booking_date));
    }
}

if (!function_exists("readableDate")) {
    function readableDate($date)
    {
        return date("D, d M Y", strtotime($date));
    }
}

if (!function_exists("readableDateTime")) {
    function readableDateTime($datetime)
    {
        return date("D, d M Y H:m", strtotime($datetime));
    }
}

if (!function_exists("thousandSeparator")) {
    function thousandSeparator($price)
    {
        return number_format($price, 0, ",", ".");
    }
}

if (!function_exists('generateRandomCode')) {
    function generateRandomCode($length = 50)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' . md5(uniqid(rand(), true));
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('isValidMd5')) {
    function isValidMd5($md5 = '')
    {
        return preg_match('/^[a-f0-9]{32}$/', $md5);
    }
}

if (!function_exists('getAlphabetByIndex')) {
    function getAlphabetByIndex($index)
    {
        $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        return $alphabet[$index];
    }
}

if (!function_exists('getMemutuskanList')) {
    function getMemutuskanList($index)
    {
        $putusanList = array('KESATU', 'KEDUA', 'KETIGA', 'KEEMPAT', 'KELIMA', 'KEENAM', 'KETUJUH', 'KEDELAPAN', 'KESEMBILAN', 'KESUPULUH', 'KESEBELAS', 'KEDUABELAS', 'KETIGABELAS');
        return $putusanList[$index];
    }
}

if (!function_exists('clean')) {
    function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-.]/', '_', $string); // Removes special chars.
    }
}

if (!function_exists('insertNotification')) {
    function insertNotification($request_surat_id, $user_id, $message, $created_by)
    {
        $notification = new NotificationModel();
        $notification->request_surat_id = $request_surat_id;
        $notification->user_id = $user_id;
        $notification->message = $message;
        $notification->created_by = $created_by;
        $notification->created_date = now();
        $notification->dbInsert($notification->fetch());
    }
}

if (!function_exists('markReadNotificationPrev')) {
    function markReadNotificationPrev($request_surat_id)
    {
        $notification = new NotificationModel();
        $notification->is_viewed = 1;
        $notification->updated_date = now();
        $notification->dbUpdateArry($notification->fetch(), array('request_surat_id' => $request_surat_id, 'is_viewed' => 0));
    }
}
