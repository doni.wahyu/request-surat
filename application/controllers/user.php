<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
    var $title = "NEW CUSTOMER";
    var $cUri = "user";
    var $menuName = "Customer";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
    }

    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'CUSTOMER LIST';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $user = new UserModel();
        $dataTable = $user->dbGetAll();
        $data['dataTable'] = $dataTable;

        $this->load->view('HeaderView', $data);
        $this->load->view('UserView', $data);
        $this->load->view('FooterView', $data);
    }

    public function upateProfile()
    {
        $id = $this->input->post('user_id');

        if ($id) {
            $user = new UserModel();
            $user->nip = $this->input->post('nip');
            $user->nama_lengkap = $this->input->post('nama_lengkap');
            $user->email = $this->input->post('email');
            $user->no_hp = $this->input->post('no_hp');
            $user->jabatan = $this->input->post('jabatan');
            $user->profile_pic = $this->input->post('profile_pic');

            if (!isValidMd5($this->input->post('password'))) {
                $user->password = md5($this->input->post('password'));
            }

            if ($id) {
                $user->updated_by = getSessionUserId();
                $user->updated_date = now();
            } else {
                $user->is_account_verified = 1;
                $user->created_by = getSessionUserId();
                $user->created_date = now();
            }

            if ($user->dbUpdate($user->fetch(), $id)) {
                $this->session->set_userdata('profile_pic', $user->profile_pic);

                $res = array('success' => true, 'message' => ' Profile berhasil diupdate');
            } else {
                $res = array('success' => false, 'message' => ' Profile gagal diupdate');
            }

            printJson($this, $res);
        }
    }


    public function profile($id = null)
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Profil';

        $breadcrumb = "<li class='active'>Profil</li>";
        $data['breadcrumb'] = $breadcrumb;

        $user = new UserModel();

        if ($id == null) {
            $data['model'] = null;
        } else {
            $data['model'] = $user->getById($id);
        }

        $this->load->view('HeaderView', $data);
        $this->load->view('ProfileFormView', $data);
        $this->load->view('FooterView', $data);
    }

    public function getAll()
    {
        $pageno = $_GET['pageno'];
        $search_query = $_GET['search_query'];

        $no_of_records_per_page = 10;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $user = new UserModel();

        $query = "select user_id, fullname, member_type_name, user.is_active, user.member_type_id from user
                  left join member_type on user.member_type_id = member_type.member_type_id
                  where 1 = 1 and role = 'USER'";

        if ($search_query != '') {
            $query .= " AND (fullname LIKE '%$search_query%')";
        }

        $query .= " LIMIT $offset, $no_of_records_per_page";

        $data = $user->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }

    public function getAllCustomer()
    {
        $query_filter = $_GET['query'];

        $user = new UserModel();
        $query = "select user_id, fullname from user where 1 = 1 and role = 'USER'";

        if ($query != '') {
            $query .= " AND fullname LIKE '%$query_filter%'";
        }

        $query .= " order by fullname limit 25";

        $userList = $user->dbGetRows($query);

        $dataList = array();

        foreach ($userList as $item) {
            if (strlen($query) > 0) {
                array_push($dataList, array(
                    'value' => $item->fullname,
                    'data' => $item->user_id));
            }
        }

        $res = array('query' => 'Unit', 'suggestions' => $dataList);
        printJson($this, $res);
    }

    public function getAllNonMemberOnly()
    {
        $query_filter = $_GET['query'];

        $user = new UserModel();
        $query = "select user_id, fullname from user where 1 = 1 and role = 'USER' and member_type_id is null";

        if ($query != '') {
            $query .= " AND fullname LIKE '%$query_filter%'";
        }

        $query .= " order by fullname limit 25";

        $userList = $user->dbGetRows($query);

        $dataList = array();

        foreach ($userList as $item) {
            if (strlen($query) > 0) {
                array_push($dataList, array(
                    'value' => $item->fullname,
                    'data' => $item->user_id));
            }
        }

        $res = array('query' => 'Unit', 'suggestions' => $dataList);
        printJson($this, $res);
    }

    public function getAllMemberOnly()
    {
        $query_filter = $_GET['query'];

        $user = new UserModel();
        $query = "select user_id, fullname from user where 1 = 1 and role = 'USER' and member_type_id is not null";

        if ($query != '') {
            $query .= " AND fullname LIKE '%$query_filter%'";
        }

        $query .= " order by fullname limit 25";

        $userList = $user->dbGetRows($query);

        $dataList = array();

        foreach ($userList as $item) {
            if (strlen($query) > 0) {
                array_push($dataList, array(
                    'value' => $item->fullname,
                    'data' => $item->user_id));
            }
        }

        $res = array('query' => 'Unit', 'suggestions' => $dataList);
        printJson($this, $res);
    }

    public function form($id = null)
    {
        $action = ($id == null) ? 'Add' : 'Edit';
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
        $data['breadcrumb'] = $breadcrumb;

        $user = new UserModel();

        if ($id == null) {
            $data['model'] = null;
        } else {
            $data['model'] = $user->getById($id);
        }

        $this->load->view('HeaderView', $data);
        $this->load->view('UserFormView', $data);
        $this->load->view('FooterView', $data);
    }

    public function save()
    {
        $id = $this->input->post('user_id');

        $user = new UserModel();
        $user->fullname = $this->input->post('fullname');
        $user->address = $this->input->post('address');
        $user->email = $this->input->post('email');
        $user->phone_number = $this->input->post('phone_number');
        $user->note = $this->input->post('note');
        $user->register_date = $this->input->post('register_date');
        $user->emergency_contact = $this->input->post('emergency_contact');
        $user->emergency_phone_number = $this->input->post('emergency_phone_number');

        if ($id) {
            $user->updated_by = getSessionUserId();
            $user->updated_date = now();
        } else {
            $user->is_account_verified = 1;
            $user->created_by = getSessionUserId();
            $user->created_date = now();
        }

        if ($id) {
            if ($user->dbUpdate($user->fetch(), $id)) {
                $res = array('success' => true, 'message' => $this->menuName . ' updated', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => $this->menuName . ' fail update');
            }
        } else {
            if ($user->dbInsert($user->fetch())) {
                $res = array('success' => true, 'message' => $this->menuName . ' inserted', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => $this->menuName . ' fail insert');
            }
        }

        printJson($this, $res);
    }

    public function getById($id)
    {
        $response = array();

        if ($id) {
            $user = new UserModel();
            $userList = $user->dbGetRows("select user.*, member_type.member_type_name from user
                                            left join member_type on user.member_type_id = member_type.member_type_id
                                            where user_id = $id");

            $response = count($userList) > 0 ? $userList[0] : NULL;
        }

        printJson($this, $response);
    }

    public function reactive($id = null)
    {
        if ($id) {
            $user = new UserModel();
            $user->is_active = 1;

            if ($user->dbUpdate($user->fetch(), $id)) {
                $res = array('success' => true, 'message' => 'Reactive Successfully', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => 'Reactive fail');
            }
        } else {
            $res = array('success' => false, 'message' => 'Reactive Gagal');
        }
        printJson($this, $res);
    }
}