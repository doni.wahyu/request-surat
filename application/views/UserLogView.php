    <style>
    table {
        font-size: 12px;
    }

    #custom-search-input {
        padding: 3px;
        border: solid 1px #E4E4E4;
        background-color: #fff;
    }

    #custom-search-input input {
        border: 0;
        box-shadow: none;
    }

    #custom-search-input button {
        margin: 2px 0 0 0;
        background: none;
        box-shadow: none;
        border: 0;
        color: #666666;
        padding: 0 8px 0 10px;
        border-left: solid 1px #ccc;
    }

    #custom-search-input button:hover {
        border: 0;
        box-shadow: none;
        border-left: solid 1px #ccc;
    }

    #custom-search-input .glyphicon-search {
        font-size: 14px;
    }

    .link-blue {
        font-weight: bold;
        color: #2860ff;
        text-decoration: none;
        cursor: pointer;
    }
</style>
<br>
<div class="row">
    <div class="col-md-5">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" name="search_query" id="search_query" class="form-control" placeholder="Fullname / Email"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
            </div>
        </div>
    </div>

    <div class="col-md-2" style="margin-top: 5px">
        <div class="input-group">
            <input class="form-control date-picker"
                   id="login_date" name="login_date" placeholder="Login Date"
                   type="text" required
                   readonly/>
            <div class="input-group-addon icon-calendar">
                <i class="fa fa-calendar">
                </i>
            </div>
        </div>
    </div>

    <div class="col-md-2" style="margin-top: 5px">
        <button type="button" class="btn btn-primary" onclick="resetAndSearchData()">SEARCH</button>
    </div>
</div>

<br>
<div id="response">
    <table id="table-user" class="table table-striped">
        <thead>
        <tr style="background-color: #f2f2f2">
            <th>Fullname</th>
            <th>Email</th>
            <th>Role</th>
            <th>Login In</th>
            <th>Log Out</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<input type="hidden" id="pageno" value="0">
<img id="loader" src="<?= base_url() ?>/assets/img/loader.svg">

<script>
    $(document).ready(function () {
        searchData();

        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                searchData();
            }
        });


        $('#search_query').keypress(function (e) {
            if (e.which == 13) {
                resetAndSearchData();
            }
        });

    });

    function resetAndSearchData() {
        $('#pageno').val("0");
        $("#table-user").find("tr:gt(0)").remove();
        searchData();
    }

    function searchData() {
        var nextPage = parseInt($('#pageno').val()) + 1;
        $.ajax({
            type: 'GET',
            url: '<?= $cUri ?>/getUserlogList',
            data: {
                pageno: nextPage
                , search_query: $('#search_query').val()
                , login_date: $('#login_date').val()
            },
            success: function (result) {
                if (result.data.length > 0) {

                    $i = 0;
                    $.each(result.data, function () {
                        var color = ($i % 2 == 0) ? '' : 'active';
                        var logout = (this.logout_date == null) ? '' : this.logout_date;

                        $html = '<tr class="' + color + '"><td>' + this.fullname + '</td><td>' + this.email + '</td>' +
                            '<td>' + this.role + '</td><td>' + this.login_date + '</td><td>' + logout + '</td></tr>';

                        $('#table-user tr:last').after($html);
                        $('#pageno').val(nextPage);

                        $i++;
                    });
                }

                $("#loader").hide();
            }
        });
    }

</script>