<br>
<form id="data" role="form" method="post">
    <div class="row">
        <div class="col-md-4">
            <p style="font-size: 14px;font-weight: bold">PERSONAL INFORMATION</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->user_id : '' ?>"
                   class="hidden" id="user_id" name="user_id">

            <input type="text" value="<?= ($model) ? $model->fullname : '' ?>"
                   class="form-control" id="fullname" name="fullname" placeholder="Customer Name" required/>
        </div>
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->address : '' ?>"
                   class="form-control" id="address" name="address" placeholder="Address" required/>
        </div>
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->phone_number : '' ?>"
                   class="form-control numeric" id="phone_number" name="phone_number" placeholder="Phone Number" required/>
        </div>
    </div>

    <div class="row top20">
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->email : '' ?>"
                   class="form-control" id="email" name="email" placeholder="Email" required/>
        </div>
    </div>

    <div class="row top20">
        <div class="col-md-4">
            <textarea class="form-control" id="note" name="note" placeholder="Customer Note"><?= ($model) ? $model->note : '' ?></textarea>
        </div>
    </div>

    <div class="row top20">
        <div class="col-md-4">
            <p style="font-size: 14px;font-weight: bold">EMERGENCY CONTACT</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->emergency_contact : '' ?>"
                   class="form-control" id="emergency_contact" name="emergency_contact"
                   placeholder="Contact Name"/>
        </div>
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->emergency_phone_number : '' ?>"
                   class="form-control numeric" id="emergency_phone_number" name="emergency_phone_number"
                   placeholder="Contact Phone"/>
        </div>
    </div>

    <div class="row top20">
        <div class="col-md-4">
            <p style="font-size: 14px;font-weight: bold">REGISTER INFORMATION</p>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="input-group">
                <input class="form-control date-picker"
                       value="<?= ($model) ? $model->register_date : '' ?>"
                       id="register_date" name="register_date" placeholder="Register Date" type="text" required
                       readonly/>
                <div class="input-group-addon icon-calendar">
                    <i class="fa fa-calendar">
                    </i>
                </div>
            </div>
        </div>
    </div>

    <div class="row top10 hide">
        <div class="col-md-4">
            <select class="form-control" onchange="toggleTipeAnggota()" id="member_type_id" name="member_type_id">
                <option value="">Tipe Angota</option>
                <?
                foreach ($memberTypeList as $memberType) {
                    ?>
                    <option value="<?= $memberType->member_type_id ?>"
                        <?php
                        if ($model) {
                            if ($memberType->member_type_id == $model->member_type_id) {
                                echo "selected";
                            }
                        }
                        ?>
                    ><?= $memberType->member_type_name ?></option>
                    <?
                }
                ?>
            </select>
        </div>
        <div class="col-md-4" id="div_total_quota" style="display: none">
            <input type="text" value="<?= ($model) ? $model->total_quota : '' ?>" class="form-control numeric"
                   id="total_quota" name="total_quota"
                   placeholder="Total Quota">
            <?
            if ($model) {
                ?>
                <div class="help">Sisa Quota</div>
                <?
            }
            ?>
        </div>
        <div class="col-md-4" id="div_day_quota" style="display: none">
            <div class="input-group">
                <input class="form-control date-picker"
                       value="<?= ($model) ? $model->day_quota : '' ?>"
                       id="day_quota" name="day_quota" placeholder="Masa Aktif" type="text"
                       readonly/>
                <div class="input-group-addon icon-calendar">
                    <i class="fa fa-calendar">
                    </i>
                </div>
            </div>
            <?
            if ($model) {
                ?>
                <div class="help">Masa Aktif Quota</div>
                <?
            }
            ?>
        </div>
    </div>

    <div class="row top30">
        <div class="col-md-4">
            <button type="button" class="btn btn-primary" onclick="doSave()">SUBMIT</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var validator = $('#data').validate();

    function toggleTipeAnggota() {
        if ($('#member_type_id :selected').text() == 'NON MEMBER') {
            $('#div_total_quota').hide();
            $('#div_day_quota').hide();
            $('#total_quota').val('');
            $('#day_quota').val('');
        } else {
            $('#div_total_quota').show();
            $('#div_day_quota').show();
        }
    }

    function doSave() {
        if ($('#data').valid()) {
            $.blockUI();
            var dataForm = $('#data').serialize();
            $.ajax({
                url: '<?=$cUri?>/save',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    bootbox.alert(data.message, function () {
                        location.href = data.redirect;
                    });
                    $.unblockUI();
                },
                error: function (response) {
                    message(response.responseText);
                    $.unblockUI();
                }
            });
        }
        else {
            validator.focusInvalid();
            return false;
        }
    }

    <?
    if($model) {
    if($model->member_type_id != 1) {
    ?>
    $('#div_total_quota').show();
    $('#div_day_quota').show();
    <?
    }
    }
    ?>
</script>
