<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller
{
    var $title = "REPORT";
    var $cUri = "report";
    var $menuName = "Report";

    function __construct()
    {
        parent::__construct();
        $this->load->model('RequestSuratModel');
        $this->load->model('JenisSuratModel');
        $this->load->model('RequestSuratDetailModel');
        $this->load->model('AttachmentModel');
        $this->load->model('UserModel');
        $this->load->model('RequestSuratAssignModel');
    }

    public function autodispatch()
    {
        $requestSurat = new RequestSuratAssignModel();

        $minDate = date('Y-m-d', strtotime("-3 days"));
        $query = "select * from request_surat_assign where followup_date is null and point is NULL and DATE(assign_date) <= '$minDate'";

        $pendingList = $requestSurat->dbGetRows($query);

        foreach ($pendingList as $pending) {
            $requestSuratAssign = new RequestSuratAssignModel();
            $requestSuratAssign->point = DECREASE_POINT;
            $requestSuratAssign->dbUpdate($requestSuratAssign->fetch(), $pending->request_surat_assign_id);

            $user = new UserModel();
            $user = $user->dbGetRows("select * from user where role = 'STAFF' and user_id <> " . $pending->user_id);

            $assignsurat = new RequestSuratAssignModel();
            $assignsurat->request_surat_id = $pending->request_surat_id;
            $assignsurat->user_id = $user[0]->user_id;
            $assignsurat->assign_date = now();
            $assignsurat->dbInsert($assignsurat->fetch());
        }
    }

    public function getAll()
    {
        $search_query = $_GET['search_query'];
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];

        $requestSurat = new RequestSuratModel();

        $queryFilterDate = '';
        if ($start_date <> '' && $end_date <> '') {
            $queryFilterDate = " AND DATE(assign_date) BETWEEN '$start_date' AND '$end_date'";
        } else if ($start_date <> '') {
            $queryFilterDate = " AND DATE(assign_date) >= '$start_date'";
        } else if ($end_date <> '') {
            $queryFilterDate = " AND DATE(assign_date) <= '$end_date'";
        }

        $query = "select nama_lengkap, total_point_process, total_point_unprocess, total_point_process - total_point_unprocess as total_point from user
	inner join (
	select sum(point) as total_point_process, user_id from request_surat_assign where followup_date is not null $queryFilterDate
	group by user_id ) as process_surat on process_surat.user_id = user.user_id

	inner join (
	select sum(point) as total_point_unprocess, user_id from request_surat_assign where followup_date is null $queryFilterDate
	group by user_id ) as unprocess_surat on unprocess_surat.user_id = user.user_id
	WHERE 1 = 1
	";

        if ($search_query != '') {
            $query .= " AND nama_lengkap LIKE '%$search_query%'";
        }

        $query .= " order by nama_lengkap";

        $data = $requestSurat->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }

}