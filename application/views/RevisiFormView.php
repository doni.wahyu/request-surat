<p style="color: #063a69;font-size: 16px;font-weight: bold;">Verifikasi Surat</p>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
            <tr>

            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Jenis Surat</th>
                <td><?= $jenisSurat->jenis_name ?></td>
            </tr>
            <tr>
                <th scope="row">Nomor Surat</th>
                <td><?= $requestSurat->no_surat ?></td>
            </tr>
            <tr>
                <th scope="row">Judul Surat</th>
                <td><?= $requestSurat->judul_surat ?></td>
            </tr>

            </tbody>
        </table>
    </div>
    <div class="col-md-4">

    </div>

</div>

<form id="data" role="form" method="post">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="mydata">
                <thead class="bg-primary">
                <tr>
                    <th style="width: 100px;" scope="col">Detail</th>
                    <th style="width: 500px;" scope="col">Isi</th>
                    <th scope="col">Revisi</th>
                    <th style="width: 100px;" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($requestSuratDetailList as $row) {
                    ?>
                    <tr>
                        <td><?= strtoupper($row->type_detail) ?></td>
                        <td><textarea class="form-control" style="height: 60px" name="isi"
                                      placeholder="Isi"><?= $row->isi; ?></textarea></td>
                        <td><?= $row->comment; ?>
                        </td>
                        <td>
                            <?php
                            if ($requestSurat->status == 'REJECTED') {
                                ?>
                                <button type="button" class="btn btn-info"
                                        onclick="doRevisiIsi(this, <?= $row->request_surat_detail_id; ?>)">SUBMIT
                                </button>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>


    <?php
    if ($requestSurat->status == 'REJECTED') {
        ?>
        <div class="row top20">
            <div class="col-md-2">

                <input type="hidden" id="request_surat_id" name="request_surat_id" value="<?= $request_surat_id ?>">
                <button type="button" style="width: 100%" class="btn btn-success" onclick="doSubmitRevisi()">SUBMIT
                    REVISI
                </button>
            </div>
        </div>
        <?php
    }
    ?>
</form>


<script type="text/javascript">
    var validator = $('#data').validate();

    function doRevisiIsi(obj, request_surat_detail_id) {
        var isi = $(obj).parent().prev().prev().children(":first").val().trim();

        if (isi == '') {
            $(obj).parent().prev().children(":first").focus()
        } else {
            if ($('#data').valid()) {
                $.blockUI();
                $.ajax({
                    url: '<?=$cUri?>/revisiIsi',
                    type: 'POST',
                    data: {
                        request_surat_detail_id: request_surat_detail_id,
                        isi: isi,
                    },
                    success: function (data) {
                        if (data.success) {
                            bootbox.alert(data.message, function () {
                                window.location.reload();
                            });
                        } else {
                            message(data.message)
                        }
                        $.unblockUI();
                    },
                    error: function (response) {
                        message(response.responseText);
                        $.unblockUI();
                    }
                });
            }
            else {
                validator.focusInvalid();
                return false;
            }
        }
    }

    function doSubmitRevisi() {
        bootbox.confirm("Anda yakin akan mensubmit revisi ?", function (result) {
            if (result) {
                if ($('#data').valid()) {
                    $.blockUI();
                    var dataForm = $('#data').serialize();
                    $.ajax({
                        url: '<?=$cUri?>/doSubmitRevisi',
                        type: 'POST',
                        data: dataForm,
                        success: function (data) {
                            if (data.success) {
                                bootbox.alert(data.message, function () {
                                    location.href = data.redirect;
                                });
                            } else {
                                message(data.message)
                            }
                            $.unblockUI();
                        },
                        error: function (response) {
                            message(response.responseText);
                            $.unblockUI();
                        }
                    });
                }
                else {
                    validator.focusInvalid();
                    return false;
                }
            }
        });
    }


</script>
