<script>
    $('.date-picker').datepicker({
        autoclose: true,
        allowInputToggle: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });

    $('.icon-calendar').click(function () {
        $(this).prev('.date-picker').datepicker('show');
    });

    $(".numeric").keypress(function (e) {
        if (e.which != 46 && e.which != 45 && e.which != 46 && !(e.which >= 48 && e.which <= 57)) {
            return false;
        }
    });

    /*$('.numeric').keyup(function () {
     if (e.which != 46 && e.which != 45 && e.which != 46 &&
     !(e.which >= 48 && e.which <= 57)) {
     return false;
     }
     });*/

    $('select.selectpicker').selectpicker({
        caretIcon: 'glyphicon glyphicon-menu-down'
    });

    function toast(message) {
        $('#snackbar').html(message);

        var x = document.getElementById("snackbar");
        x.className = "show";
        // After 3 seconds, remove the show class from DIV
        setTimeout(function () {
            x.className = x.className.replace("show", "");
        }, 2000);
    }

    $('.clockpicker').clockpicker();

    getTotalUnReadSurat();
    function getTotalUnReadSurat() {
        $.getJSON('<?=base_url()?>requestsurat/getTotalUnReadSurat', function (result) {
            $('#totalUnReadSurat').html(result.totalUnread);
        });

        $.ajax({
            url: '<?=base_url()?>requestsurat/getDataUnReadSurat',
            type: 'GET',
            success: function (data) {
                $('#notification-dropdown-menu').replaceWith(data);
            },
            error: function (response) {
            }
        });
    }

    setInterval(function () {
        getTotalUnReadSurat()
    }, 1000 * 10);

    function openSurat() {
        window.location = '<?=base_url('surat')?>';
    }
</script>
</div> <!--content-->
</div> <!--wrapper-->
</body>
</html>
