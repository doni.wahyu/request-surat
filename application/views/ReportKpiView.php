<br>
<div class="alert alert-success" role="alert">
    <p>Jika surat diproses point bertambah (<?=INCREASE_POINT?>)</p>
    <p>Jika surat terlewat point berkurang (<?=DECREASE_POINT?>)</p>
</div>

<div class="row">
    <div class="col-md-4">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" name="search_query" id="search_query" class="form-control"
                       placeholder="Nama Pegawai"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
            </div>
        </div>
    </div>

    <div class="col-md-2" style="padding-top: 5px">
        <div class="input-group">
            <input class="form-control date-picker"
                   id="start_date" name="start_date" placeholder="Start Date"
                   type="text" readonly/>
            <div class="input-group-addon icon-calendar">
                <i class="fa fa-calendar">
                </i>
            </div>
        </div>
    </div>

    <div class="col-md-2" style="padding-top: 5px">
        <div class="input-group">
            <input class="form-control date-picker"
                   id="end_date" name="end_date" placeholder="End Date"
                   type="text" readonly/>
            <div class="input-group-addon icon-calendar">
                <i class="fa fa-calendar">
                </i>
            </div>
        </div>
    </div>
</div>

<br>
<div id="response">
    <table id="table-surat" class="table table-striped">
        <thead>
        <tr style="background-color: #e7f3ff">
            <th>Nama</th>
            <th>Processed (Point)</th>
            <th>Unprocessed (Point)</th>
            <th>Toatl Point</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<input type="hidden" id="pageno" value="0">
<img id="loader" src="<?= base_url() ?>/assets/img/loader.svg">

<script>
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');
    var isLoadData = false;

    $(document).ready(function () {
        searchData();

        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                searchData();
            }
        });

        $('#search_query').keypress(function (e) {
            if (e.which == 13) {
                resetAndSearchData();
            }
        });

        $('#start_date').change(function (e) {
            resetAndSearchData();
        });

        $('#end_date').change(function (e) {
            resetAndSearchData();
        });

    });

    function resetAndSearchData() {
        $('#pageno').val("0");
        $("#table-surat").find("tr:gt(0)").remove();
        searchData();
    }

    function searchData() {
        if (!isLoadData) {
            isLoadData = true;

            var nextPage = parseInt($('#pageno').val()) + 1;
            $.ajax({
                type: 'GET',
                url: '<?= $cUri ?>/getAll',
                data: {
                    pageno: nextPage, search_query: $('#search_query').val(), start_date: $('#start_date').val(),
                    end_date: $('#end_date').val(),
                },
                success: function (result) {
                    isLoadData = false;

                    if (result.data.length > 0) {

                        $i = 0;
                        $.each(result.data, function () {
                            var color = (this.is_viewed == 0) ? 'unview' : 'white';

                            $html = '<tr class="' + color + '"><td>' + this.nama_lengkap + '</td>' +
                                '<td>' + this.total_point_process + '</td>' +
                                '<td>' + this.total_point_unprocess + '</td>' +
                                '<td>' + this.total_point + '</td></tr>';


                            $('#table-surat tr:last').after($html);
                            $('#pageno').val(nextPage);

                            $i++;
                        });
                    }

                    $("#loader").hide();
                }
            });
        }
    }

    function doEdit(request_surat_id) {
        window.location = '<?=base_url($this->cUri)?>/form/' + request_surat_id;
    }


    function disposisi(request_surat_id) {
        window.location = '<?=base_url($this->cUri)?>/disposisi/' + request_surat_id;
    }

</script>