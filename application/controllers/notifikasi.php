<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends MY_Controller
{
    var $title = "Notifikasi";
    var $cUri = "notifikasi";
    var $menuName = "Notifikasi";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('RequestSuratModel');
    }

    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from surat";
        $data['totalSurat'] = $surat->dbGetRow($query)->total;


        $surat = new RequestSuratModel();
        $query = "select count(*) as total from surat where pic in ('KASUBBAG', 'JFU')";
        $data['disposisiSurat'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from surat where status <> 'DISETUJI'";
        $data['pendinganSurat'] = $surat->dbGetRow($query)->total;

        $this->load->view('HeaderView', $data);
        $this->load->view('NotifikasiView');
        $this->load->view('FooterView', $data);
    }

    public function getPosisiSurat($search = '')
    {
        $user = new UserModel();
        $kasubbagList = $user->getUserByRole('KASUBBAG');

        $html = '<div class="row top10">';

        $suratKabag = new RequestSuratModel();
        $query = "select no_surat, perihal, user.nama_lengkap, jabatan from disposisi
                    inner join (
                    select max(disposisi_id) as disposisi_id, surat_id from disposisi group by surat_id) a
                    on a.disposisi_id = disposisi.disposisi_id
                    inner join surat on surat.surat_id = disposisi.surat_id
                    inner join user on user.user_id = disposisi.user_id
                    where user.role = 'KABAG' and user.is_active = 1 and perihal like '%$search%' limit 10";

        $suratList = $suratKabag->dbGetRows($query);

        $html .= '<div class="col-md-3">
                    <p style="color: #063a69">KABAG</p>
                    <table class="table table-striped top10">
                        <thead>
                            <tr style="background-color: #e7f3ff">
                                <th>Surat</th>
                            </tr>
                        </thead>
                        <tbody>';

        foreach ($suratList as $surat) {
            $html .= ' <tr>
                            <td>' . $surat->perihal . '</td>
                        </tr>';
        }

        $html .= '</tbody>
                    </table>
                    </div>';

        foreach ($kasubbagList as $jfu) {
            $surat = new RequestSuratModel();

            $query = "select no_surat, perihal, user.nama_lengkap, jabatan from disposisi
                        inner join (
                        select max(disposisi_id) as disposisi_id, surat_id from disposisi group by surat_id) a
                        on a.disposisi_id = disposisi.disposisi_id
                        inner join surat on surat.surat_id = disposisi.surat_id
                        inner join user on user.user_id = disposisi.user_id
                        where (disposisi.user_id in (select user_id from user where spv_id = " . $jfu->user_id . ")
                         or disposisi.user_id = " . $jfu->user_id . ")
                         and perihal like '%$search%' limit 10";

            $suratList = $surat->dbGetRows($query);

            $html .= '<div class="col-md-3">
                    <p style="color: #063a69">' . $jfu->jabatan . '</p>
                    <table class="table table-striped top10">
                        <thead>
                            <tr style="background-color: #e7f3ff">
                                <th>Surat</th>
                            </tr>
                        </thead>
                        <tbody>';

            foreach ($suratList as $surat) {
                $html .= ' <tr>
                            <td>' . $surat->perihal . '</td>
                        </tr>';
            }

            $html .= '</tbody>
                    </table>
                    </div>';
        }

        $html .= '</div>';
        echo $html;
    }

}