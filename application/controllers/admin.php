<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{
    var $title = "USER";
    var $cUri = "admin";
    var $menuName = "User";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
    }

    public function userlog()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'USER LOGS';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $user = new UserModel();
        $dataTable = $user->dbGetAll();
        $data['dataTable'] = $dataTable;

        $this->load->view('HeaderView', $data);
        $this->load->view('UserLogView', $data);
        $this->load->view('FooterView', $data);
    }

    public function getUserlogList()
    {
        $pageno = $_GET['pageno'];
        $search_query = $_GET['search_query'];
        $login_date = $_GET['login_date'];

        $no_of_records_per_page = 10;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $user = new UserModel();

        $query = "select email, nama_lengkap, role, user_activity.* from user inner join user_activity
                    on user.user_id = user_activity.user_id where role <> 'USER'";

        if ($search_query != '') {
            $query .= " AND (nama_lengkap LIKE '%$search_query%' or email LIKE '%$search_query%')";
        }

        if ($login_date != '') {
            $query .= " AND login_date like '%$login_date%'";
        }

        $query .= " order by user_activity_id desc LIMIT $offset, $no_of_records_per_page";

        $data = $user->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }


    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'DAFTAR USER';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $user = new UserModel();
        $dataTable = $user->dbGetAll();
        $data['dataTable'] = $dataTable;

        $this->load->view('HeaderView', $data);
        $this->load->view('AdminView', $data);
        $this->load->view('FooterView', $data);
    }

    public function getAll()
    {
        $pageno = $_GET['pageno'];
        $search_query = $_GET['search_query'];

        $no_of_records_per_page = 50;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $user = new UserModel();

        $query = "select * from user where 1 = 1 ";

        if ($search_query != '') {
            $query .= " AND (nama_lengkap LIKE '%$search_query%' or nip LIKE '%$search_query%')";
        }

        $query .= " LIMIT $offset, $no_of_records_per_page";

        $data = $user->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }

    public function form($id = null)
    {
        $action = ($id == null) ? 'Add' : 'Edit';
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = ($id == null) ? 'TAMBAH ' : 'EDIT ' . $this->title;

        $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
        $data['breadcrumb'] = $breadcrumb;

        $user = new UserModel();

        if ($id == null) {
            $data['model'] = null;
        } else {
            $data['model'] = $user->getById($id);
        }

        $userExisting = new UserModel();
        $data['spvList'] = $userExisting->dbGetRows("select * from user where role <> 'ADMIN'");

        $this->load->view('HeaderView', $data);
        $this->load->view('AdminFormView', $data);
        $this->load->view('FooterView', $data);
    }

    public function save()
    {
        $id = $this->input->post('user_id');
        $nip = $this->input->post('nip');
        $email = $this->input->post('email');

        $user = new UserModel();
        $user->nama_lengkap = $this->input->post('nama_lengkap');
        $user->email = $email;
        $user->nip = $nip;
        $user->role = $this->input->post('role');
        $user->is_active = $this->input->post('is_active');

        if (!isValidMd5($this->input->post('password'))) {
            $user->password = md5($this->input->post('password'));
        }

        if ($id) {
            $user->updated_by = getSessionUserId();
            $user->updated_date = now();
        } else {
            $user->created_by = getSessionUserId();
            $user->created_date = now();
        }

        if ($id) {
            if ($user->dbUpdate($user->fetch(), $id)) {
                $res = array('success' => true, 'message' => $this->menuName . ' berhasil diupdate', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => $this->menuName . ' gagla diupdate');
            }
        } else {
            $userNip = new UserModel();
            $userEmail = new UserModel();

            if ($userNip->isNipExist($nip) || $userEmail->isEmailExist($email)) {
                $res = array('success' => false, 'message' => $this->menuName . ' email / nip sudah tersedia');
            } else {
                if ($user->dbInsert($user->fetch())) {
                    $res = array('success' => true, 'message' => $this->menuName . ' berhasil ditambah', 'redirect' => base_url($this->cUri));
                } else {
                    $res = array('success' => false, 'message' => $this->menuName . ' gagal ditambah');
                }
            }
        }

        printJson($this, $res);
    }
}