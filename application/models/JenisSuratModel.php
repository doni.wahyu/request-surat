<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class JenisSuratModel extends BaseModel
{
    function JenisSuratModel()
    {
        parent::__construct();
        $this->tableName = 'jenis_surat';
        $this->primaryKeyName = 'jenis_surat_id';
    }
}