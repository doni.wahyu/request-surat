<div class="row">

    <div class="col-md-8">
        <p style="color: #063a69;font-size: 16px;font-weight: bold;">DASHBOARD PETUGAS</p>
        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #367bf5, #36c7f5)">
                    <div style="font-size: 14px;font-weight: bold">Total Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalSurat ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i style="position: absolute; right: 15px; bottom: 15px" class="fa fa-arrow-down"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #ed5454, #f8a406)">
                    <div style="font-size: 14px;font-weight: bold">Disposisi Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalAssigned ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i style="position: absolute; right: 15px; bottom: 15px" class="fa fa-arrow-up"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #2fa84f, #85e259)">
                    <div style="font-size: 14px;font-weight: bold">Approve Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalApproved ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i style="position: absolute; right: 15px; bottom: 15px" class="fa fa-check"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row" style="padding: 10px;">
                <div class="col-md-12"
                     style="border-radius:5px; padding:15px;color: white;background-image: linear-gradient(to top, #ed5454, #FD4744)">
                    <div style="font-size: 14px;font-weight: bold">Pendingan Surat</div>
                    <div style="font-size: 30px;font-weight: bold"><?= $totalRequested ?></div>
                    <div style="font-size: 20px;margin-top: 5px"><i style="position: absolute; right: 15px; bottom: 15px" class="fa fa-hourglass-end"></i></div>
                </div>
            </div>
        </div>
    </div>

</div>