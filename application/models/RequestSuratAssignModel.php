<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class RequestSuratAssignModel extends BaseModel
{
    function RequestSuratAssignModel()
    {
        parent::__construct();
        $this->tableName = 'request_surat_assign';
        $this->primaryKeyName = 'request_surat_assign_id';
    }

    function getLastUser($request_surat_id)
    {
        $this->db->where('request_surat_id', $request_surat_id);
        $this->db->order_by('request_surat_assign_id', 'desc');
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }

}