<form id="data" role="form" method="post">
    <p style="color: #063a69;font-size: 16px;font-weight: bold;">Edit Request Surat</p>
    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Jenis Surat SK :</div>
            <input type="text" value="<?= ($model) ? $model->request_surat_id : '' ?>"
                   class="hidden" id="request_surat_id" name="request_surat_id">

            <select class="form-control" id="jenis_surat_id" name="jenis_surat_id" required>
                <option value="">--</option>
                <?
                foreach ($jenissuratList as $value) {
                    ?>
                    <option
                        value="<?= $value->jenis_surat_id ?>"<?= ($model) ? ($model->jenis_surat_id == $value->jenis_surat_id ? 'selected' : '') : ''; ?>>
                        <?= $value->jenis_name ?>
                    </option>
                    <?
                }
                ?>

            </select>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">

            <div class="help">Nomor Surat :</div>
            <input type="text" value="<?= ($model) ? $model->no_surat : '' ?>"
                   class="form-control" id="no_surat" name="no_surat" placeholder="Nomor Surat"/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">

            <div class="help">Judul Surat :</div>
            <textarea class="form-control" style="height: 60px" id="judul_surat" name="judul_surat"
                      placeholder="Judul Surat"><?= ($model) ? $model->judul_surat : '' ?></textarea>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">

            <div class="help">Nota Dinas :</div>
            <input type="text" value="<?= ($model) ? $model->nota_dinas : '' ?>"
                   class="form-control" id="nota_dinas" name="nota_dinas" placeholder="Nota Dinas"/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4" style="padding-top: 5px">
            <div class="help">Tanggal Surat :</div>
            <div class="input-group">
                <input class="form-control date-picker" value="<?= ($model) ? $model->tanggal_surat : '' ?>"
                       id="tanggal_surat" name="tanggal_surat" placeholder="Tanggal Surat"
                       type="text" readonly/>
                <div class="input-group-addon icon-calendar">
                    <i class="fa fa-calendar">
                    </i>
                </div>
            </div>
        </div>
    </div>

    <div class="row top20">
        <div class="col-md-2">
            <button type="button" style="width: 100%" class="btn btn-info top5" onclick="doSave()">SUBMIT</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');
    var validator = $('#data').validate();

    function doSave() {
        if ($('#data').valid()) {
            $.blockUI();
            var dataForm = $('#data').serialize();
            $.ajax({
                url: '<?=$cUri?>/edit',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    if (data.success) {
                        bootbox.alert(data.message, function () {
                            location.href = data.redirect;
                        });
                    } else {
                        message(data.message)
                    }
                    $.unblockUI();
                },
                error: function (response) {
                    message(response.responseText);
                    $.unblockUI();
                }
            });
        }
        else {
            validator.focusInvalid();
            return false;
        }
    }
</script>
