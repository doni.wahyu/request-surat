<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('UserActivityModel');
    }

    public function index()
    {
        $data['title'] = "Login";
        if (!isLoggedIn()) {
            $this->load->view('LoginView', $data);
        } else {
            if (getSessionRole() == 'ADMIN' || getSessionRole() == 'SUPER ADMIN') {
                $redirect = '/dashboard/admin';
            } else if (getSessionRole() == 'REQUESTER') {
                $redirect = '/dashboard/requester';
            } else if (getSessionRole() == 'STAFF') {
                $redirect = '/dashboard/staff';
            }
            redirect(base_url($redirect));
        }
    }

    public function doLogin()
    {
        $res = "";
        if ($this->input->post('email') && $this->input->post('password')) {

            $user = new UserModel();
            $user = $user->getUserByEmailAndPassword($this->input->post('email'), $this->input->post('password'));

            if ($user == NULL) {
                $res = array('success' => false, 'message' => 'Email / NIP / Password tidak sesuai');
            } else {
                $session_id = uniqid();
                $ip_address = $_SERVER['REMOTE_ADDR'];
                $user_agent = $_SERVER['HTTP_USER_AGENT'];

                $keycode = generateRandomCode();

                $userActivity = new UserActivityModel();
                $userActivity->user_Id = $user->user_id;
                $userActivity->device_info = $user_agent;
                $userActivity->keycode = $keycode;
                $userActivity->login_date = now();
                $userActivity->dbInsert($userActivity->fetch());

                $data = array(
                    'session_id' => $session_id,
                    'app_session_id' => $session_id,
                    'user_id' => $user->user_id,
                    'email' => $user->email,
                    'nama_lengkap' => $user->nama_lengkap,
                    'keycode' => $keycode,
                    'role' => $user->role,
                    'jabatan' => $user->jabatan,
                    'profile_pic' => $user->profile_pic,
                );

                $this->session->set_userdata($data);

                if ($user->role == 'ADMIN' || $user->role == 'SUPER ADMIN') {
                    $redirect = '/dashboard/admin';
                } else if ($user->role == 'REQUESTER') {
                    $redirect = '/dashboard/requester';
                } else if ($user->role == 'STAFF') {
                    $redirect = '/dashboard/staff';
                }

                $res = array('success' => true, 'redirect' => base_url($redirect));
            }
        }

        printJson($this, $res);
    }

    function logout()
    {
        $keycode = getSessionKeycode();
        $userActivity = new UserActivityModel();
        $userActivity->logout($keycode);
        $this->session->sess_destroy();
        $this->index();
    }

}
