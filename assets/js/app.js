function thousandSeparator(x) {
    if (x != null) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    } else {
        return '-';
    }
}

function convertSize(size) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (size == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
    return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function blink(selector) {
    $(selector).fadeOut('slow', function () {
        $(this).fadeIn('slow', function () {
            blink(this);
        });
    });
}

blink('.blink');

function generateAttributeValueHtml(objId, attributeId, intValue) {
    var html = '';
    $.getJSON(BASE_URL + 'attributevalue/getByAttributeId/' + attributeId, function (result) {
        if (result.data.length == 0) {
            html = '<input value="' + (intValue == undefined ? '' : intValue) + '" type="number" min="0" id="intValue" name="intValue" placeholder="Value" class="col-xs-2 col-sm-2" required style="margin-left: 10px">';
        } else {
            html = '<select id="intValue" name="intValue" class="col-xs-4 col-sm-4" style="margin-left: 10px">';

            $.each(result.data, function () {
                html += '<option value="' + this.minValue + '" ' + (intValue == undefined ? '' : (intValue == this.minValue ? 'selected' : '' )) + '>' + this.valueName + '</option>';
            });

            html += '</select>';
        }
        $('#' + objId).html(html);
    });
}

/**
 * Created by doni.wahyu on 3/23/2017.
 */

$(window).load(function () {
    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
    })

    $('.icon-calendar').click(function () {
        $(this).prev('.date-picker').datepicker('show');
    })
});


/*$(window).bind('load resize', function () {
 $("#grid-table").setGridWidth($(".page-content").width());
 }).trigger('load resize');*/

//replace icons with FontAwesome icons like above
function updatePagerIcons(table) {
    var replacement =
    {
        'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
        'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
        'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
        'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
    };
    $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
        var icon = $(this);
        var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

        if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
    })
}

function formatterEdit(cellvalue, options, rowObject) {
    return "<button class='btn btn-info btn-xs' title='View / Edit selected row' onclick=\"doEdit(" + cellvalue + ")\" > <i class='ace-icon fa fa-pencil bigger-110 icon-only'></i></button>";
}

function formatterUrlEdit(cellvalue, options, rowObject) {
    return "<a href='#' onclick=\"doEdit(" + cellvalue + ")\" >" + cellvalue + "</a>";
    //return "<button class='btn btn-info btn-xs' title='View / Edit selected row' onclick=\"doEdit(" + cellvalue + ")\" > <i class='ace-icon fa fa-pencil bigger-110 icon-only'></i></button>";
}

function message(output_msg) {
    /*$('#alert_wrap').fadeIn();
     $('#alert_content').text(output_msg);
     setTimeout(function () {
     $('#alert_wrap').hide();
     $('#alert_content').text('');
     }, 10000);*/
    bootbox.alert(output_msg, function () {
     });
}

var Constants = {
    ACTION_ADD: 'ADD',
    ACTION_EDIT: 'EDIT',
    ACTION_SEARCH: 'SEARCH',
    STATUS_CREATED: 'CREATED',
    STATUS_SUBMITTED: 'SUBMITTED',
    STATUS_APPROVED: 'APPROVED',
    STATUS_REJECTED: 'REJECTED',
    STATUS_FINISHED: 'FINISHED',
    STATUS_LULUS: 'LULUS',
    STATUS_TIDAK_LULUS: 'TIDAK LULUS'
}

function toggleButtonActionForm(state) {
    if (state == Constants.ACTION_ADD || state == Constants.ACTION_EDIT) {
        $('#btn_submit').show();
        $('#btn_reset').show();
        $('#btn_search').hide();
        $('#btn_clear_search').hide();

        if (state == Constants.ACTION_ADD) {
            $('#btn_submit').html('<i class="ace-icon fa fa-check bigger-110"></i>Save');
        } else {
            $('#btn_submit').html('<i class="ace-icon fa fa-check bigger-110"></i>Update');
        }
    } else if (state == Constants.ACTION_SEARCH) {
        $('#btn_submit').hide();
        $('#btn_reset').hide();
        $('#btn_search').show();
        $('#btn_clear_search').show();
        $('#data').trigger('reset');
        $("#data").deserialize(searchValue);
    }
}