
<br>
<div class="row">
    <div class="col-md-5">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" name="search_query" id="search_query" class="form-control" placeholder="Nama / NIP"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
            </div>
        </div>
    </div>
</div>

<br>
<div id="response">
    <table id="table-user" class="table table-striped">
        <thead>
        <tr style="background-color: #f2f2f2">
            <th>Nama Lengkap</th>
            <th>NIP</th>
            <th>Role</th>
            <th>Status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<input type="hidden" id="pageno" value="0">
<img id="loader" src="<?= base_url() ?>/assets/img/loader.svg">

<script>
    var isLoadData = false;

    $(document).ready(function () {
        searchData();

        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                searchData();
            }
        });


        $('#search_query').keypress(function (e) {
            if (e.which == 13) {
                resetAndSearchData();
            }
        });

    });

    function resetAndSearchData() {
        $('#pageno').val("0");
        $("#table-user").find("tr:gt(0)").remove();
        searchData();
    }

    function searchData() {
        if (!isLoadData) {
            isLoadData = true;

            var nextPage = parseInt($('#pageno').val()) + 1;
            $.ajax({
                type: 'GET',
                url: '<?= $cUri ?>/getAll',
                data: {
                    pageno: nextPage, search_query: $('#search_query').val()
                },
                success: function (result) {
                    isLoadData = false;

                    if (result.data.length > 0) {

                        $i = 0;
                        $.each(result.data, function () {
                            var color = ($i % 2 == 0) ? '' : 'active';
                            var classColor = (this.is_active == 1) ? '' : 'color-red';
                            var statusUser = (this.is_active == 1) ? 'Active' : 'In-Active';


                            $html = '<tr class="' + color + '"><td>' + this.nama_lengkap + '</td><td>' + this.nip + '</td>' +
                                '<td>' + this.role + '</td><td>' + statusUser + '</td><td><a class="link-blue" href="#" onclick="doEdit(' + this.user_id + ')">EDIT</a></td></tr>';

                            $('#table-user tr:last').after($html);
                            $('#pageno').val(nextPage);

                            $i++;
                        });
                    }

                    $("#loader").hide();
                }
            });
        }
    }

    function doEdit(user_id) {
        window.location = '<?=base_url($this->cUri)?>/form/' + user_id;
    }

    function reactive() {
        bootbox.confirm("Anda yakin akan Re-active (" + $('#nama_anggota').text() + ")?", function (result) {
            if (result) {
                $.ajax({
                    type: 'GET', url: '<?=$cUri?>/reactive/' + $('#user_id').val(),
                    success: function (data) {
                        bootbox.alert(data.message, function () {
                            location.href = data.redirect;
                        });
                        $.unblockUI();
                    },
                    error: function (response) {
                        message(response.responseText);
                        $.unblockUI();
                    }
                });

            }
        });
    }

</script>