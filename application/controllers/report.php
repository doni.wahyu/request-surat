<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller
{
    var $title = "REPORT";
    var $cUri = "report";
    var $menuName = "Report";

    function __construct()
    {
        parent::__construct();
        $this->load->model('RequestSuratModel');
        $this->load->model('JenisSuratModel');
        $this->load->model('RequestSuratDetailModel');
        $this->load->model('AttachmentModel');
        $this->load->model('UserModel');
        $this->load->model('RequestSuratAssignModel');
    }

    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Report KPI';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $jenissurat = new JenisSuratModel();
        $data['jenissuratList'] = $jenissurat->dbGetAll();

        $this->load->view('HeaderView', $data);
        $this->load->view('ReportKpiView', $data);
        $this->load->view('FooterView', $data);
    }

    public function getAll()
    {
        $search_query = $_GET['search_query'];
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];

        $requestSurat = new RequestSuratModel();

        $queryFilterDate = '';
        if ($start_date <> '' && $end_date <> '') {
            $queryFilterDate = " AND DATE(assign_date) BETWEEN '$start_date' AND '$end_date'";
        } else if ($start_date <> '') {
            $queryFilterDate = " AND DATE(assign_date) >= '$start_date'";
        } else if ($end_date <> '') {
            $queryFilterDate = " AND DATE(assign_date) <= '$end_date'";
        }

        $query = "select nama_lengkap, ifnull(total_point_process, 0) as total_point_process, ifnull(total_point_unprocess, 0) as total_point_unprocess,
	ifnull(total_point_process,0) - ifnull(total_point_unprocess,0) as total_point from user

	left join (
	select sum(point) as total_point_process, user_id from request_surat_assign where followup_date is not null $queryFilterDate
	group by user_id ) as process_surat on process_surat.user_id = user.user_id

	left join (
	select sum(point) as total_point_unprocess, user_id from request_surat_assign where followup_date is null $queryFilterDate
	group by user_id ) as unprocess_surat on unprocess_surat.user_id = user.user_id
	WHERE 1 = 1 and user.role = 'STAFF'
	";

        if ($search_query != '') {
            $query .= " AND nama_lengkap LIKE '%$search_query%'";
        }

        $query .= " order by nama_lengkap";

        $data = $requestSurat->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }

}