<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class AttachmentModel extends BaseModel
{
    function AttachmentModel()
    {
        parent::__construct();
        $this->tableName = 'attachment';
        $this->primaryKeyName = 'attachment_id';
    }

    function getByRequestSurat($request_surat_id)
    {
        $this->db->where('request_surat_id', $request_surat_id);
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
}