<style>
    .upload-area {
        background-color: #cacaca;
        width: 100%;
        height: 180px;
        border: 2px solid lightgray;
        border-radius: 3px;
        margin: 0 auto;
        text-align: center;
        overflow: hidden;
    }

    .upload-area:hover {
        cursor: pointer;
    }

    .upload-area h1 {
        text-align: center;
        font-weight: normal;
        font-family: sans-serif;
        line-height: 50px;
        color: darkslategray;
    }

    .alert, .progress {
        display: none;
        margin-bottom: 10px;
    }
</style>
<form id="data" role="form" method="post">
    <div class="row top20">
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->user_id : '' ?>"
                   class="hidden" id="user_id" name="user_id">

            <div class="help">NIP :</div>
            <input type="text" value="<?= ($model) ? $model->nip : '' ?>"
                   class="form-control" id="nip" name="nip" placeholder="NIP"
                   required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Jabatan :</div>
            <input type="text" value="<?= ($model) ? $model->jabatan : '' ?>"
                   class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan"
                   required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Nama Lengkap :</div>
            <input type="text" value="<?= ($model) ? $model->nama_lengkap : '' ?>"
                   class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap"
                   required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Emnail / Username Login :</div>
            <input type="text" value="<?= ($model) ? $model->email : '' ?>"
                   class="form-control" id="email" name="email" placeholder="Email"
                   required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Password :</div>
            <input type="password" value="<?= ($model) ? $model->password : '' ?>"
                   class="form-control" id="password" name="password" placeholder="Password"
                   required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">No Hp :</div>
            <input type="text" value="<?= ($model) ? $model->no_hp : '' ?>"
                   class="form-control" id="no_hp" name="no_hp" placeholder="No Hp"/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Photo :</div>

            <?
            if ($model) {
                if ($model->profile_pic != null && $model->profile_pic != '') {
                    ?>
                    <img id="profile_pic_img"
                         style="border: 0.5px solid lightgrey; width: 160px; height: 160px"
                         src="<?= base_url(UPLOAD_DATA_DIR . '/' . $model->profile_pic) ?>">
                    <?
                } else {
                    ?>
                    <img id="profile_pic_img"
                         style="border: 0.5px solid lightgrey; width: 160px; height: 160px"
                         src="<?= base_url() ?>assets/img/person.jpg">
                    <?
                }
            }
            ?>
        </div>
        <div class="col-md-4">
            <input type="file" name="userfile" id="userfile" style="display: none">
            <div class="upload-area" id="uploadfile" style="display: table;">
                <input type="text" value="<?= ($model) ? $model->profile_pic : '' ?>"
                       class="hidden" id="profile_pic" name="profile_pic">

                <div style="top:50%;height: 100%;;display: table-cell;vertical-align: middle;">
                    <i class="material-icons md-light md-80">cloud_upload</i>
                    <h4 style="color: #b90d09;">Pilih Photo</h4>
                    <h4 style="color: #757575;">atau geser photo ke area ini</h4>
                </div>
            </div>

            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                     style="width: 0%;"></div>
            </div>
            <div class="alert alert-success" role="alert">Upload Berhasil, tekan update melakukan perubahan</div>
        </div>
    </div>

    <div class="row top20">
        <div class="col-md-2">
            <button type="button" style="width: 100%" class="btn btn-warning" onclick="doSave()">UPDATE</button>
        </div>
    </div>
</form>

<script src="<?= base_url() ?>assets/js/jquery.form.js"></script>

<script type="text/javascript">
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');
    var validator = $('#data').validate();

    function doSave() {
        if ($('#data').valid()) {
            $.blockUI();
            var dataForm = $('#data').serialize();
            $.ajax({
                url: '<?=$cUri?>/upateProfile',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    if (data.success) {
                        bootbox.alert(data.message, function () {
                            window.location.reload();
                        });
                    } else {
                        message(data.message)
                    }
                    $.unblockUI();
                },
                error: function (response) {
                    message(response.responseText);
                    $.unblockUI();
                }
            });
        }
        else {
            validator.focusInvalid();
            return false;
        }
    }

    $(function () {
        // preventing page from redirecting
        $("html").on("dragover", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("h1").text("Drag here");
        });

        $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        // Drag enter
        $('.upload-area').on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drag over
        $('.upload-area').on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drop
        $('.upload-area').on('drop', function (e) {
            e.stopPropagation();
            e.preventDefault();

            $("h1").text("Upload");

            var file = e.originalEvent.dataTransfer.files;
            var fd = new FormData();
            fd.append('userfile', file[0]);
            uploadData(fd);
        });

        // Open file selector on div click
        $("#uploadfile").click(function () {
            $("#userfile").click();
        });

        // file selected
        $("#userfile").change(function () {
            var fd = new FormData();
            var files = $('#userfile')[0].files[0];
            fd.append('userfile', files);
            uploadData(fd);
        });
    });

    function uploadData(formdata) {
        console.log('starting ajax request');
        $progress.css('display', 'block');
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $progressBar.css('width', (percentComplete * 100) + '%');

                        if (percentComplete == 1) {
                            $progress.css('display', 'none');
                            $alert.css('display', 'block');
                        }
                    }
                }, false);
                //Download progress
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        console.log(percentComplete);
                        $progressBar.css('width', (percentComplete * 100) + '%');

                        if (percentComplete == 1) {
                            $progress.css('display', 'none');
                            $alert.css('display', 'block');
                        }
                    }
                }, false);
                return xhr;
            },
            url: '<?=base_url()?>image/upload',
            type: 'post',
            data: formdata,
            contentType: false,
            processData: false,
            success: function (response) {
                $('#profile_pic').val(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message(errorThrown);
            }
        });
    }
</script>
