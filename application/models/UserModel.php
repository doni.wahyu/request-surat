<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends BaseModel
{
    function UserModel()
    {
        parent::__construct();
        $this->tableName = 'user';
        $this->primaryKeyName = 'user_id';
    }

    function getCustomer()
    {
        return $this->dbGetRows("select * from user where role = 'USER'");
    }


    function getUser($id)
    {
        $data = array();
        $this->db->where($this->primaryKeyName, $id);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
        }
        return $data;
    }

    function isEmailExist($email)
    {
        $this->db->where('email', $email);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);

        return ($query->num_rows() > 0) ? true : false;
    }

    function isNipExist($nip)
    {
        $this->db->where('nip', $nip);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);

        return ($query->num_rows() > 0) ? true : false;
    }

    function isPhoneNumber($phoneNumber)
    {
        $this->db->where('phone_number', $phoneNumber);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);

        return ($query->num_rows() > 0) ? true : false;
    }

    function getUserByEmail($email)
    {
        $data = array();
        $this->db->where('email', $email);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);

        if ($query->num_rows() > 0) {
            $data = $query->row();
        }
        return $data;
    }

    function getUserByKey($key)
    {
        $data = array();
        $this->db->where('password', $key);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);

        if ($query->num_rows() > 0) {
            $data = $query->row();
        }
        return $data;
    }


    function findByLoginEmail()
    {
        $this->db->where('email', getSessionUserId());
        $query = $this->db->get($this->tableName);

        $result = $query->result_array();

        return (count($result) > 0 ? $result[0] : NULL);
    }

    function getUserByEmailAndPassword($email, $password)
    {
        $passwordMD5 = md5($password);
        $query = "select * from user where (email = '$email' and password = '$passwordMD5')
              or (nip = '$email' and password = '$passwordMD5')";
        $result = $this->db->query($query)->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }


    function getActiveUser()
    {
        $this->db->where('is_active', 1);
        $query = $this->db->get($this->tableName);
        $this->db->order_by('fullname', 'asc');
        return $query->result();
    }

    function countAll($query)
    {
        $this->db->select('COUNT(*) AS RES');
        $this->db->from('user');
        $this->db->join('member_type', 'user.member_type_id = member_type.member_type_id', 'LEFT');

        if (count($query) > 0) {
            $this->db->or_like(array('fullname' => $query, 'member_type_name' => $query));
        }

        $res = $this->db->get()->result();
        return $res[0]->RES;
    }

    function getAll($query, $sort, $order, $limit, $page)
    {
        $start = $limit * $page - $limit;
        if ($start < 0) $start = 0;

        $this->db->select('user_id, fullname, member_type_name');
        $this->db->from('user');
        $this->db->join('member_type', 'user.member_type_id = member_type.member_type_id', 'LEFT');

        if (count($query) > 0) {
            $this->db->or_like(array('fullname' => $query, 'member_type.member_type_name' => $query));
        }

        $this->db->order_by($sort, $order);
        $this->db->limit($limit, $start);

        return $this->db->get()->result();
    }

    function getUserByRole($role)
    {
        $this->db->where('role', $role);
        $this->db->where('is_active', 1);
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    function getUserBySpv($user_id)
    {
        $this->db->where('spv_id', $user_id);
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
}