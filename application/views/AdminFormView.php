<br>
<form id="data" role="form" method="post">
    <div class="row">
        <div class="col-md-4">
            <input type="text" value="<?= ($model) ? $model->user_id : '' ?>"
                   class="hidden" id="user_id" name="user_id">

            <div class="help">Nama Lengkap :</div>
            <input type="text" value="<?= ($model) ? $model->nama_lengkap : '' ?>"
                   class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap" required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">NIP :</div>
            <input type="text" value="<?= ($model) ? $model->nip : '' ?>"
                   class="form-control" id="nip" name="nip" placeholder="NIP" required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Email :</div>
            <input type="text" value="<?= ($model) ? $model->email : '' ?>"
                   class="form-control" id="email" name="email" placeholder="Email" required/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Password :</div>
            <input type="password" value="<?= ($model) ? $model->password : '' ?>" id="password" name="password"
                   class="form-control" placeholder="Password" class="col-xs-10 col-sm-10" required>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Role :</div>
            <select class="form-control" id="role" name="role" required>
                <option value="">Role</option>
                <option value="ADMIN" <?= ($model) ? ($model->role == "ADMIN" ? 'selected' : '') : ''; ?>>ADMIN</option>
                <option value="SUPER ADMIN" <?= ($model) ? ($model->role == "SUPER ADMIN" ? 'selected' : '') : ''; ?>>SUPER ADMIN</option>
                <option value="REQUESTER" <?= ($model) ? ($model->role == "REQUESTER" ? 'selected' : '') : ''; ?>>REQUESTER</option>
                <option value="STAFF" <?= ($model) ? ($model->role == "STAFF" ? 'selected' : '') : ''; ?>>STAFF</option>
            </select>
        </div>
    </div>


    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Status :</div>
            <select id="is_active" name="is_active" class="form-control" required>
                <option value="1" <?= ($model) ? ($model->is_active == 1 ? 'selected' : '') : ''; ?>>Active
                </option>
                <option value="0" <?= ($model) ? ($model->is_active == 0 ? 'selected' : '') : ''; ?>>In-Active
                </option>
            </select>
        </div>
    </div>

    <div class="row top20">
        <div class="col-md-2">
            <button type="button" style="width: 100%" class="btn btn-warning" onclick="doSave()">SUBMIT</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var validator = $('#data').validate();

    function doSave() {
        if ($('#data').valid()) {
            $.blockUI();
            var dataForm = $('#data').serialize();
            $.ajax({
                url: '<?=$cUri?>/save',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    if (data.success) {
                        bootbox.alert(data.message, function () {
                            location.href = data.redirect;
                        });
                    } else {
                        message(data.message)
                    }
                    $.unblockUI();
                },
                error: function (response) {
                    message(response.responseText);
                    $.unblockUI();
                }
            });
        }
        else {
            validator.focusInvalid();
            return false;
        }
    }
</script>
