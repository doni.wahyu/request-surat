<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Report Page</title>

    <style>
        .center {
            text-align: center;
        }

        .table-finger {
            border-collapse: collapse;
            border: 1px solid black;
        }

        body {
            font-size: 12px;
        }

        .table-header h1, h2, h3, h4, h5 {
            line-height: 0.3em;
        }

        body {
            font-family: "Times New Roman", Times, serif;
        }

        .table-detail td {
            vertical-align: text-top;
        }
    </style>
</head>
<body>

<?php
if ($surat->jenis_surat_id == 1) {
    ?>
    <table style="width:100%;" class="table-header">
        <tr>
            <td class="center" style="width: 10%;">
                <img style="width: 80px; height: 100px;margin-top: 5px"
                     src="<?= base_url() ?>assets/img/logo.png">
            </td>
            <td class="center" style="width: 80%;">
                <h2>KEMENTRIAN DALAM NEGERI</h2>
                <h2>REPUBLIK INDONESIA</h2>
                <h2>DIREKTORAT JENDRAL</h2>
                <h2>BINA PEMERINTAHAN DESA</h2>
                <h4>Jl.Rraya Pasar Minggu Km. 19 Jakarta Selamatan 12072, Telp. (021) 7942373-74</h4>
            </td>
            <td class="center" style="width: 10%;">
            </td>
        </tr>
    </table>

    <div style="margin-top: 20px; width:100%; height: 6px; background-color: black"></div>
    <div style="margin-top:3px; width:100%; height: 1px; background-color: black"></div>

    <h3 class="center" style="margin-top: 20px;">KEPUTUSAN KUASA PENGGUNA ANGGARAN</h3>
    <h3 class="center">NOMOR <?= strtoupper($surat->no_surat) ?></h3>
    <h3 style="margin-top: 30px" class="center">TENTANG</h3>
    <h3 style="margin-top: 30px;line-height: normal" class="center"><?= strtoupper(nl2br($surat->judul_surat)) ?></h3>

    <h3 style="margin-top: 30px" class="center">KUASA PENGGUNA ANGGARAN</h3>
    <h3 class="center">DIREKTORAT JENDERAL BINA PEMERINTAHAN DESA,</h3>
    <?php
} else {
    ?>
    <table style="width:100%;" class="table-header">
        <tr>
            <td class="center" style="width: 100%;">
                <img style="width: 100px; height: 100px;margin-top: 5px"
                     src="<?= base_url() ?>assets/img/garuda.png">
            </td>
        </tr>
    </table>

    <h3 class="center">KEPUTUSAN KUASA PENGGUNA ANGGARAN</h3>
    <h3 class="center">NOMOR <?= strtoupper($surat->no_surat) ?></h3>
    <h3 style="margin-top: 30px" class="center">TENTANG</h3>
    <h3 style="margin-top: 30px;line-height: normal" class="center"><?= strtoupper(nl2br($surat->judul_surat)) ?></h3>

    <h3 style="margin-top: 30px" class="center">MENTERI DALAM NEGERI,</h3>
    <?
}
?>

<table style="width:100%;margin-top: 30px;" class="table-detail">
    <tr>
        <td style="width: 19%;">
            Menimbang
        </td>
        <td style="width: 1%;">
            :
        </td>
        <td style="width: 80%;">
            <table style="width:100%; ">
                <?php

                $i = 0;
                foreach ($suratDetailList as $suratDetail) {
                    if ($suratDetail->type_detail == 'menimbang') {
                        ?>

                        <tr>
                            <td><?= getAlphabetByIndex($i) ?>.</td>
                            <td style="text-align: justify"><?= $suratDetail->isi ?></td>
                        </tr>

                        <?php
                        $i++;
                    }
                }
                ?>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width: 19%;">
            Mengingat
        </td>
        <td style="width: 1%;">
            :
        </td>
        <td style="width: 80%;">
            <table style="width:100%; ">
                <?php

                $i = 1;
                foreach ($suratDetailList as $suratDetail) {
                    if ($suratDetail->type_detail == 'mengingat') {
                        ?>

                        <tr>
                            <td><?= $i ?>.</td>
                            <td style="text-align: justify"><?= $suratDetail->isi ?></td>
                        </tr>

                        <?php
                        $i++;
                    }
                }
                ?>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width: 19%;">
            Memperhatikan
        </td>
        <td style="width: 1%;">
            :
        </td>
        <td style="width: 80%;">
            <table style="width:100%; ">
                <?php

                $i = 1;
                foreach ($suratDetailList as $suratDetail) {
                    if ($suratDetail->type_detail == 'memperhatikan') {
                        ?>

                        <tr>
                            <td><?= $i ?>.</td>
                            <td style="text-align: justify"><?= $suratDetail->isi ?></td>
                        </tr>

                        <?php
                        $i++;
                    }
                }
                ?>
            </table>
        </td>
    </tr>
</table>
<h3 class="center">MEMUTUSKAN</h3>
<table style="width:100%;margin-top: 30px;" class="table-detail">
    <tr>
        <td style="width: 19%;">
            Menetapkan
        </td>
        <td style="width: 1%;">
            :
        </td>
        <td style="width: 80%;">
        </td>
    </tr>
</table>

<table style="width:100%;margin-top: 10px;" class="table-detail">
    <?php

    $i = 1;
    foreach ($suratDetailList as $suratDetail) {
        if ($suratDetail->type_detail == 'memutuskan') {
            ?>

            <tr>
                <td style="width: 19%;">
                    <?= getMemutuskanList($i) ?>
                </td>
                <td style="width: 1%;">
                    :
                </td>
                <td style="width: 80%;text-align: justify"><?= $suratDetail->isi ?></td>
            </tr>

            <?php
            $i++;
        }
    }
    ?>
</table>

<table style="width:100%;" class="table-header">
    <tr>
        <td style="width: 60%;">
        </td>
        <td style="width: 40%;">
            Ditetapkan di Jakarta<br>
            pada tanggal

            <h3 style="line-height: normal">
                KUASA PENGGUNAN ANGGARAN <br>DIREKTORA JENDERAL <br>BINA PEMERINTAHAN DESA,
            </h3>

            <br>
            <br>
            <br>
            <br>
            <br>
            <h3>MOHAMMAD RIZAL</h3>
        </td>
    </tr>
</table>

<br>

<table style="width: 100%">
    <tr>
        <td width="70%">
            Salinan Keputusan ini disampaikan Kepada Yth. :<br>
            1. Direkur Jenderal Bina Pemerintahan Desa (sebagai laporan);<br>
            2. Badan Pemeriksa Keuangan RI di Jakarta;<br>
            3. Inspektorat Jenderal Kementerian Dalam Negeri;<br>
            4. Kepala Kantor Wilayah Direktorat Jenderal Perbendaharaan DKI Jakarta;<br>
            5. Kepala Kantor Pelayanan Perbendaharaan Negara (KPPN) Jakarta IV;<br>
            6. Yang bersangkutan.
        </td>
        <td width="30%" style="text-align: right">
            <img style="width: 120px; height: 120px;margin-top: 5px"
                 src="<?= base_url() ?><?= $qrCodePath ?>">
        </td>
    </tr>
</table>
<br>

<script>
    window.print();
</script>
</body>
</html>