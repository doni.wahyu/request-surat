<style>
    .upload-area {
        background-color: #cacaca;
        width: 100%;
        height: 180px;
        border: 2px solid lightgray;
        border-radius: 3px;
        margin: 0 auto;
        text-align: center;
        overflow: hidden;
    }

    .upload-area:hover {
        cursor: pointer;
    }

    .upload-area h1 {
        text-align: center;
        font-weight: normal;
        font-family: sans-serif;
        line-height: 50px;
        color: darkslategray;
    }

    .alert, .progress {
        display: none;
        margin-bottom: 10px;
    }


</style>

<form id="data" role="form" method="post">
    <p style="color: #063a69;font-size: 16px;font-weight: bold;">Request Surat</p>
    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Jenis Surat SK :</div>
            <input type="text" value="<?= ($model) ? $model->request_surat_id : '' ?>"
                   class="hidden" id="request_surat_id" name="request_surat_id">

            <select class="form-control" id="jenis_surat_id" name="jenis_surat_id" required>
                <option value="">--</option>
                <?
                foreach ($jenissuratList as $value) {
                    ?>
                    <option
                        value="<?= $value->jenis_surat_id ?>"<?= ($model) ? ($model->jenis_surat_id == $value->jenis_surat_id ? 'selected' : '') : ''; ?>>
                        <?= $value->jenis_name ?>
                    </option>
                    <?
                }
                ?>

            </select>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">

            <div class="help">Nomor Surat :</div>
            <input type="text" value="<?= ($model) ? $model->no_surat : '' ?>"
                   class="form-control" id="no_surat" name="no_surat" placeholder="Nomor Surat"/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">

            <div class="help">Judul Surat :</div>
            <textarea class="form-control" style="height: 60px" id="judul_surat" name="judul_surat"
                      placeholder="Judul Surat"><?= ($model) ? $model->judul_surat : '' ?></textarea>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">

            <div class="help">Nota Dinas :</div>
            <input type="text" value="<?= ($model) ? $model->nota_dinas : '' ?>"
                   class="form-control" id="nota_dinas" name="nota_dinas" placeholder="Nota Dinas"/>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4" style="padding-top: 5px">
            <div class="help">Tanggal Surat :</div>
            <div class="input-group">
                <input class="form-control date-picker"
                       id="tanggal_surat" name="tanggal_surat" placeholder="Tanggal Surat"
                       type="text" readonly/>
                <div class="input-group-addon icon-calendar">
                    <i class="fa fa-calendar">
                    </i>
                </div>
            </div>
        </div>
    </div>

    <div class="row top10">
        <div class="col-md-12">
            <div class="help">Menimbang :</div>
            <div id="div_menimbang">

                <div class="row top10 row_menimbang" id="row_menimbang">
                    <div class="col-md-11">
                                   <textarea class="form-control" style="height: 60px" id="menimbang" name="menimbang[]"
                                             placeholder="Menimbang"><?= ($model) ? $model->menimbang : '' ?></textarea>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-danger top10" onclick="removeRow(this)"><i
                                class="fa fa-trash bigger-110 icon-only"></i></button>
                    </div>

                </div>

            </div>

            <button class="btn btn-info top5" type="button" onclick="addRowField('menimbang')"><i
                    class="fa fa-plus bigger-110 icon-only"></i></button>
        </div>

    </div>


    <div class="row top10">
        <div class="col-md-12">
            <div class="help">Mengingat :</div>
            <div id="div_mengingat">

                <div class="row top10 row_mengingat" id="row_mengingat">
                    <div class="col-md-11">
                                   <textarea class="form-control" style="height: 60px" id="mengingat" name="mengingat[]"
                                             placeholder="Mengingat"><?= ($model) ? $model->menimbang : '' ?></textarea>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-danger top10" onclick="removeRow(this)"><i
                                class="fa fa-trash bigger-110 icon-only"></i></button>
                    </div>

                </div>

            </div>

            <button class="btn btn-info top5" type="button" onclick="addRowField('mengingat')"><i
                    class="fa fa-plus bigger-110 icon-only"></i></button>

        </div>
    </div>


    <div class="row top10">
        <div class="col-md-12">
            <div class="help">Memperhatikan :</div>
            <div id="div_memperhatikan">

                <div class="row top10 row_memperhatikan" id="row_memperhatikan">
                    <div class="col-md-11">
                                   <textarea class="form-control" style="height: 60px" id="memperhatikan"
                                             name="memperhatikan[]"
                                             placeholder="memperhatikan"><?= ($model) ? $model->memperhatikan : '' ?></textarea>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-danger top10" onclick="removeRow(this)"><i
                                class="fa fa-trash bigger-110 icon-only"></i></button>
                    </div>

                </div>

            </div>

            <button class="btn btn-info top5" type="button" onclick="addRowField('memperhatikan')"><i
                    class="fa fa-plus bigger-110 icon-only"></i>
            </button>

        </div>
    </div>


    <div class="row top10">
        <div class="col-md-12">
            <div class="help">Memutuskan :</div>
            <div id="div_memutuskan">

                <div class="row top10 row_memutuskan" id="row_memutuskan">
                    <div class="col-md-11">
                                   <textarea class="form-control" style="height: 60px" id="memutuskan"
                                             name="memutuskan[]"
                                             placeholder="memutuskan"><?= ($model) ? $model->memperhatikan : '' ?></textarea>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-danger top10" onclick="removeRow(this)"><i
                                class="fa fa-trash bigger-110 icon-only"></i></button>
                    </div>

                </div>

            </div>

            <button class="btn btn-info top5" type="button" onclick="addRowField('memutuskan')"><i
                    class="fa fa-plus bigger-110 icon-only"></i>
            </button>


        </div>
    </div>

    <div class="row top10">
        <div class="col-md-4">
            <div class="help">Attachment :</div>

        </div>
    </div>

    <div class="row top10">
        <div class="col-md-2">
            <input type="file" name="userfile" id="userfile" style="display: none">
            <div class="upload-area" id="uploadfile" style="display: table;">

                <div style="top:50%;height: 50%;;display: table-cell;vertical-align: middle;">
                    <i class="material-icons md-light md-40">cloud_upload</i>
                    <h4 style="color: #b90d09;">Attachment</h4>

                </div>
            </div>

            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                     style="width: 0%;"></div>
            </div>
            <div class="alert alert-success" role="alert">Upload Berhasil</div>
        </div>

    </div>

    <div class="row top10">
        <div class="col-md-12">
            <div id="div_file_attachment">
                <div class="row top10 row_file_attachment" id="row_file_attachment">
                    <div class="col-md-11">
                        <textarea class="form-control" style="height: 40px" id="file_attachment"
                                  name="file_attachment[]" placeholder="File Attachment ..."
                                  readonly><?= ($model) ? $model->file_attachment : '' ?></textarea>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-danger" onclick="removeRow(this)"><i
                                class="fa fa-trash bigger-110 icon-only"></i></button>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div class="row top20">
        <div class="col-md-2">
            <button type="button" style="width: 100%" class="btn btn-info top5" onclick="doSave()">SUBMIT</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');
    var validator = $('#data').validate();

    /*$('#no_surat').val('10/22/111/2020');
     $('#judul_surat').val('perintah');
     $('#tanggal_surat').val('2020-02-27');*/

    function doSave() {
        if ($('#data').valid()) {
            $.blockUI();
            var dataForm = $('#data').serialize();
            $.ajax({
                url: '<?=$cUri?>/save',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    if (data.success) {
                        bootbox.alert(data.message, function () {
                            location.href = data.redirect;
                        });
                    } else {
                        message(data.message)
                    }
                    $.unblockUI();
                },
                error: function (response) {
                    message(response.responseText);
                    $.unblockUI();
                }
            });
        }
        else {
            validator.focusInvalid();
            return false;
        }
    }

    $(function () {
        // preventing page from redirecting
        $("html").on("dragover", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("h1").text("Drag here");
        });

        $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        // Drag enter
        $('.upload-area').on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drag over
        $('.upload-area').on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drop
        $('.upload-area').on('drop', function (e) {
            e.stopPropagation();
            e.preventDefault();

            $("h1").text("Upload");

            var file = e.originalEvent.dataTransfer.files;
            var fd = new FormData();
            fd.append('userfile', file[0]);
            uploadData(fd);
        });

        // Open file selector on div click
        $("#uploadfile").click(function () {
            $("#userfile").click();
        });

        // file selected
        $("#userfile").change(function () {
            var fd = new FormData();
            var files = $('#userfile')[0].files[0];
            fd.append('userfile', files);
            uploadData(fd);
        });
    });

    function uploadData(formdata) {
        console.log('starting ajax request');
        $progress.css('display', 'block');
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $progressBar.css('width', (percentComplete * 100) + '%');

                        if (percentComplete == 1) {
                            $progress.css('display', 'none');
                            $alert.css('display', 'block');
                        }
                    }
                }, false);
                //Download progress
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        console.log(percentComplete);
                        $progressBar.css('width', (percentComplete * 100) + '%');

                        if (percentComplete == 1) {
                            $progress.css('display', 'none');
                            $alert.css('display', 'block');
                        }
                    }
                }, false);
                return xhr;
            },
            url: '<?=base_url()?>image/upload',
            type: 'post',
            data: formdata,
            contentType: false,
            processData: false,
//            dataType: 'json',
            success: function (response) {
                addRowField('file_attachment');

                setTimeout(function () {
                    $alert.hide('slow');
                    $file_attachment_list = $('textarea[name="file_attachment[]"]');

                    $i = 1;
                    $file_attachment_list.each(function () {
                        if ($i == ($file_attachment_list.length - 1)) {
                            $(this).val(response)
                        }
                        $i++;
                    });
                }, 800);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message(errorThrown);
            }
        });
    }

    function addRowField(id) {
        $("#row_" + id).clone().appendTo("#div_" + id).find("textarea").val("");
    }

    function removeRow(obj) {
        var numItems = $('.' + $(obj).parent().parent().attr('id')).length;
        if (numItems == 1) {
            message('cannot delete last row')
        } else {
            $(obj).parent().parent().remove();
        }
    }

    function removeFile(obj) {
        $(obj).parent().remove();
    }
</script>
