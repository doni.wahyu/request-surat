<br>
<div class="row">
    <div class="col-md-4">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" name="search_query" id="search_query" class="form-control"
                       placeholder="Cari No. Surat / Surat Dari"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
            </div>
        </div>
    </div>

    <div class="col-md-3" style="padding-top: 5px">
        <select class="form-control" id="jenis_surat_id" name="jenis_surat_id" required>
            <option value="">Jenis Surat</option>
            <?
            foreach ($jenissuratList as $value) {
                ?>
                <option value="<?= $value->jenis_surat_id ?>"><?= $value->jenis_name ?></option>
                <?
            }
            ?>
        </select>
    </div>

    <div class="col-md-3" style="padding-top: 5px">
        <select class="form-control" id="status" name="status" required>
            <option value="">Status</option>
            <option value="REQUESTED">REQUESTED</option>
            <option value="ASSIGNED">ASSIGNED</option>
            <option value="APPROVED">APPROVED</option>
            <option value="REJECTED">REJECTED</option>

        </select>
    </div>

    <div class="col-md-2" style="padding-top: 5px">
        <div class="input-group">
            <input class="form-control date-picker"
                   id="tanggal_surat" name="tanggal_surat" placeholder="Tanggal Surat"
                   type="text" readonly/>
            <div class="input-group-addon icon-calendar">
                <i class="fa fa-calendar">
                </i>
            </div>
        </div>
    </div>
</div>

<br>
<div id="response">
    <table id="table-surat" class="table table-striped">
        <thead>
        <tr style="background-color: #e7f3ff">
            <th>No. Surat</th>
            <th>Judul Surat</th>
            <th>Jenis</th>
            <th>Tanggal</th>
            <th>Status</th>
            <th style="text-align: center">Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<input type="hidden" id="pageno" value="0">
<img id="loader" src="<?= base_url() ?>/assets/img/loader.svg">

<div id="modal_detail" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="title_surat"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8" style="padding-top: 5px">
                        <iframe id="file_surat" src=""
                                style="width:100%; height:500px;" frameborder="0"></iframe>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="hidden" id="surat_id" name="surat_id">

                        <form id="form_proses" role="form" method="post" style="display: none">
                            <div class="row top10">
                                <div class="col-md-12">
                                    Status : <b>Proses</b>
                                </div>
                            </div>

                            <div class="row top10">
                                <div class="col-md-12">
                                    <textarea class="form-control" style="height: 60px" id="note" name="note"
                                              placeholder="Catatan"></textarea>
                                </div>
                            </div>

                            <div class="row top10">
                                <div class="col-md-12" style="font-size: 12px">
                                    Submit jika pemeriksaan telah selesai untuk disetujui atasan
                                </div>
                            </div>

                            <div class="row top10">
                                <div class="col-md-6">
                                    <button type="button" style="width: 100%" class="btn btn-warning"
                                            onclick="doProses()">SUBMIT
                                    </button>
                                </div>
                            </div>
                        </form>

                        <form id="form_approval" role="form" method="post">
                            <div class="row top10">
                                <div class="col-md-12">
                                    Status : <b>Selesai diperiksa</b>
                                </div>
                            </div>

                            <div class="row top10">
                                <div class="col-md-12">
                                    Catatan : <b id="proses_note"></b>
                                </div>
                            </div>

                            <div class="row top10">
                                <div class="col-md-6">
                                    <button type="button" style="width: 100%" class="btn btn-warning"
                                            onclick="doApprove()">SETUJU
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>
                        <p style="color: #063a69;font-size: 16px;font-weight: bold;">DOKUMEN</p>

                        <form id="form_dokumen" role="form" method="post">
                            <div class="row top10">
                                <div class="col-md-12">
                                    <input type="file" name="userfile" id="userfile" style="display: none">
                                    <div class="upload-area" id="uploadfile" style="display: table;">
                                        <input type="text" class="hidden" id="file_dokumen" name="file_dokumen">

                                        <div style="top:50%;height: 100%;;display: table-cell;vertical-align: middle;">
                                            <i class="material-icons md-light md-80">cloud_upload</i>
                                            <h4 style="color: #b90d09;">Pilih Dokumen</h4>
                                            <h4 style="color: #757575;">atau geser dokumen ke area ini</h4>
                                        </div>
                                    </div>

                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: 0%;"></div>
                                    </div>
                                    <div class="alert alert-success" role="alert">Upload Berhasil</div>
                                </div>
                            </div>

                            <div class="row top10">
                                <div class="col-md-12">
                                    <textarea class="form-control" style="height: 60px" id="note_dokumen"
                                              name="note_dokumen"
                                              placeholder="Catatan"></textarea>
                                </div>
                            </div>

                            <div class="row top10">
                                <div class="col-md-6">
                                    <button type="button" style="width: 100%" class="btn btn-warning"
                                            onclick="doSaveDokumen()">UPLOAD DOKUMEN
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>
                        <table id="table-dokumen" class="table table-striped">
                            <thead>
                            <tr style="background-color: #e7f3ff">
                                <th>Diupload Oleh</th>
                                <th>Tanggal Upload</th>
                                <th>Note</th>
                                <th>File</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <!--<div class="modal-footer">
            </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');
    var isLoadData = false;

    $(document).ready(function () {
        searchData();

        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                searchData();
            }
        });

        $('#search_query').keypress(function (e) {
            if (e.which == 13) {
                resetAndSearchData();
            }
        });

        $('#jenis_surat_id').change(function (e) {
            resetAndSearchData();
        });

        $('#sifat_surat_id').change(function (e) {
            resetAndSearchData();
        });

        $('#tanggal_surat').change(function (e) {
            resetAndSearchData();
        });

        $('#status').change(function (e) {
            resetAndSearchData();
        });
    });

    function resetAndSearchData() {
        $('#pageno').val("0");
        $("#table-surat").find("tr:gt(0)").remove();
        searchData();
    }

    function searchData() {
        if (!isLoadData) {
            isLoadData = true;

            var nextPage = parseInt($('#pageno').val()) + 1;
            $.ajax({
                type: 'GET',
                url: '<?= $cUri ?>/getAll',
                data: {
                    pageno: nextPage, search_query: $('#search_query').val(), tanggal_surat: $('#tanggal_surat').val(),
                    request_surat_id: $('#request_surat_id').val(),
                    jenis_surat_id: $('#jenis_surat_id').val(),
                    status: $('#status').val(),
                },
                success: function (result) {
                    isLoadData = false;

                    if (result.data.length > 0) {

                        $i = 0;
                        $.each(result.data, function () {
                            var color = (this.is_viewed == 0) ? 'unview' : 'white';

                            $html = '<tr class="' + color + '"><td>' + this.no_surat + '</td>' +
                                '<td>' + this.judul_surat + '</td>' +
                                '<td>' + this.jenis_name + '</td>' +
                                '<td>' + this.tanggal_surat + '</td>' +
                                '<td>' + this.status + '</td>' +
                                '<td style="text-align: center">' +
                                '<a class="link-blue" href="<?=base_url()?>requestsurat/assign/' + this.request_surat_id + '">'
                                + (this.status == 'REQUESTED' ? 'Assign' : 'View') + '</a>';

                            <? if(getSessionRole()=='SUPER ADMIN') {
                            ?>
                                $html += ' &nbsp;<a class="link-blue" href="<?=base_url()?>requestsurat/editform/' + this.request_surat_id + '">Edit</a>';
                            <? }
                            ?>

                            $html += '</td></tr>';


                            $('#table-surat tr:last').after($html);
                            $('#pageno').val(nextPage);

                            $i++;
                        });
                    }

                    $("#loader").hide();
                }
            });
        }
    }

    function doEdit(request_surat_id) {
        window.location = '<?=base_url($this->cUri)?>/form/' + request_surat_id;
    }


    function disposisi(request_surat_id) {
        window.location = '<?=base_url($this->cUri)?>/disposisi/' + request_surat_id;
    }

</script>