<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 *--------------------------------------------------------------------------
 * Date Helper Class
 * @Use Page contains functions related to date
 *--------------------------------------------------------------------------
 * @author progGroup
 * @created 08/24/2009
 * @version 1.0
 *--------------------------------------------------------------------------
 *
 */

//Code to reject direct script call
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * format_date - formats date to given date format
 */
if (!function_exists('format_date')) {
    function format_date($d, $if = 'y-m-d', $of = 'm/d/y')
    {
        if ($if == 'y-m-d') {
            $DP = explode("-", $d);
            $y = $DP[0];
            $m = $DP[1];
            $d = $DP[2];
        }

        if ($if == 'm/d/y') {
            $DP = explode("/", $d);
            $y = $DP[2];
            $m = $DP[0];
            $d = $DP[1];

        }
        if (($y == '0000') || (empty($y))) {
            return "";
        } else {

            $R = array($y, $m, $d);
            $F = array("/y/", "/m/", "/d/");

            return preg_replace($F, $R, $of);
        }
    }
}

/*
 * format_date - formats date to given date format
 */
if (!function_exists('get_oracle_date')) {
    function get_oracle_date($date)
    {
        $PARTS = explode('/', $date);

        if (count($PARTS) < 3) {
            return NULL;
        }

        switch ($PARTS[0]) {
            case 01:
                $PARTS[0] = 'JAN';
                break;
            case 1:
                $PARTS[0] = 'JAN';
                break;
            case 02:
                $PARTS[0] = 'FEB';
                break;
            case 2:
                $PARTS[0] = 'FEB';
                break;
            case 03:
                $PARTS[0] = 'MAR';
                break;
            case 3:
                $PARTS[0] = 'MAR';
                break;
            case 04:
                $PARTS[0] = 'APR';
                break;
            case 4:
                $PARTS[0] = 'APR';
                break;
            case 05:
                $PARTS[0] = 'MAY';
                break;
            case 5:
                $PARTS[0] = 'MAY';
                break;
            case 06:
                $PARTS[0] = 'JUN';
                break;
            case 6:
                $PARTS[0] = 'JUN';
                break;
            case 07:
                $PARTS[0] = 'JUL';
                break;
            case 7:
                $PARTS[0] = 'JUL';
                break;
            case '08':
                $PARTS[0] = 'AUG';
                break;
            case 8:
                $PARTS[0] = 'AUG';
                break;
            case '09':
                $PARTS[0] = 'SEP';
                break;
            case 9:
                $PARTS[0] = 'SEP';
                break;
            case 10:
                $PARTS[0] = 'OCT';
                break;
            case 11:
                $PARTS[0] = 'NOV';
                break;
            case 12:
                $PARTS[0] = 'DEC';
                break;
            default:
                return NULL;
        }

        return $PARTS[1] . "-" . $PARTS[0] . "-" . substr($PARTS[2], -2);
    }
}
?>
