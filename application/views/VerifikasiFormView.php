<p style="color: #063a69;font-size: 16px;font-weight: bold;">Verifikasi Surat</p>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
            <tr>

            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Jenis Surat</th>
                <td><?= $jenisSurat->jenis_name ?></td>
            </tr>
            <tr>
                <th scope="row">Nomor Surat</th>
                <td><?= $requestSurat->no_surat ?></td>
            </tr>
            <tr>
                <th scope="row">Judul Surat</th>
                <td><?= $requestSurat->judul_surat ?></td>
            </tr>

            <tr>
                <th scope="row">Nota Dinas</th>
                <td><?= $requestSurat->nota_dinas ?></td>
            </tr>



            </tbody>
        </table>
    </div>
    <div class="col-md-4">

    </div>

</div>

<form id="data" role="form" method="post">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="mydata">
                <thead class="bg-primary">
                <tr>
                    <th style="width: 100px;" scope="col">Detail</th>
                    <th style="width: 500px;" scope="col">Isi</th>
                    <th scope="col">Revisi</th>
                    <th style="width: 100px;" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($requestSuratDetailList as $row) {
                    ?>
                    <tr>
                        <td><?= strtoupper($row->type_detail) ?></td>
                        <td style="text-align: justify"><?= $row->isi ?></td>
                        <td>
                            <textarea class="form-control" style="height: 60px" name="revisi"
                                      placeholder="Revisi"><?= $row->comment; ?></textarea>
                        </td>
                        <td>
                            <?php
                            if ($requestSurat->status == 'ASSIGNED') {
                                ?>
                                <button type="button" class="btn btn-info"
                                        onclick="doSubmit(this, <?= $row->request_surat_detail_id; ?>)">SUBMIT
                                </button>

                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead class="bg-primary">
                <tr>
                    <th scope="col">Lampiran</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($attachmentList as $attachment) {
                    ?>
                    <tr>
                        <td>
                            <a class="link-blue" target="_blank" href="<?= base_url() . 'data/' . $attachment->file_attachment ?>"><?= $attachment->file_attachment ?></a>
                        </td>
                    </tr>
                    <?
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>


    <?php
    if ($requestSurat->status == 'ASSIGNED') {
        ?>
        <div class="row top20">
            <div class="col-md-2">

                <input type="hidden" id="request_surat_id" name="request_surat_id" value="<?= $request_surat_id ?>">
                <button type="button" style="width: 100%" class="btn btn-success" onclick="doApprove()">APPROVE</button>
            </div>

            <div class="col-md-2">
                <button type="button" style="width: 100%" class="btn btn-danger" onclick="doReject()">REJECT</button>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
    if ($requestSurat->status == 'APPROVED') {
        ?>
        <div class="row top20">
            <div class="col-md-2">
                <button type="button" style="width: 100%" class="btn btn-info" onclick="doPrint()">PRINT</button>
            </div>
        </div>
        <?php
    }
    ?>
</form>

<script>
    function doPrint() {
        window.open('<?=base_url()?>requestsurat/pdf/<?=$requestSurat->request_surat_id?>', '_blank');
    }
</script>


<script type="text/javascript">
    var validator = $('#data').validate();

    function doSubmit(obj, request_surat_detail_id) {
        var comment = $(obj).parent().prev().children(":first").val().trim();

        if (comment == '') {
            $(obj).parent().prev().children(":first").focus()
        } else {
            if ($('#data').valid()) {
                $.blockUI();
                $.ajax({
                    url: '<?=$cUri?>/submitComment',
                    type: 'POST',
                    data: {
                        request_surat_detail_id: request_surat_detail_id,
                        comment: comment,
                    },
                    success: function (data) {
                        if (data.success) {
                            bootbox.alert(data.message, function () {
                                window.location.reload();
                            });
                        } else {
                            message(data.message)
                        }
                        $.unblockUI();
                    },
                    error: function (response) {
                        message(response.responseText);
                        $.unblockUI();
                    }
                });
            }
            else {
                validator.focusInvalid();
                return false;
            }
        }
    }

    function doApprove() {
        bootbox.confirm("Anda yakin akan approve ?", function (result) {
            if (result) {
                if ($('#data').valid()) {
                    $.blockUI();
                    var dataForm = $('#data').serialize();
                    $.ajax({
                        url: '<?=$cUri?>/doApprove',
                        type: 'POST',
                        data: dataForm,
                        success: function (data) {
                            if (data.success) {
                                bootbox.alert(data.message, function () {
                                    location.href = data.redirect;
                                });
                            } else {
                                message(data.message)
                            }
                            $.unblockUI();
                        },
                        error: function (response) {
                            message(response.responseText);
                            $.unblockUI();
                        }
                    });
                }
                else {
                    validator.focusInvalid();
                    return false;
                }
            }
        });
    }

    function doReject() {
        bootbox.confirm("Anda yakin akan reject?", function (result) {
            if (result) {
                if ($('#data').valid()) {
                    $.blockUI();
                    var dataForm = $('#data').serialize();
                    $.ajax({
                        url: '<?=$cUri?>/doReject',
                        type: 'POST',
                        data: dataForm,
                        success: function (data) {
                            if (data.success) {
                                bootbox.alert(data.message, function () {
                                    location.href = data.redirect;
                                });
                            } else {
                                message(data.message)
                            }
                            $.unblockUI();
                        },
                        error: function (response) {
                            message(response.responseText);
                            $.unblockUI();
                        }
                    });
                }
                else {
                    validator.focusInvalid();
                    return false;
                }
            }
        });
    }
</script>
