<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= $title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link id="favicon" rel="shortcut icon" src="<?= base_url() ?>assets/img/favicon.png" type="image/png"/>

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome/4.5.0/css/font-awesome.min.css"/>

    <script src="<?= base_url() ?>assets/js/jquery.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.validate.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.blockUI.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.autocomplete.js"></script>
    <script src="<?= base_url() ?>assets/js/bootbox.js"></script>
    <script src="<?= base_url() ?>assets/js/app.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.dataTables.js"></script>

    <script src="<?= base_url() ?>assets/js/moment.js"></script>

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-select.css"/>
    <script src="<?= base_url() ?>assets/js/bootstrap-select.js"></script>

    <script src="<?= base_url() ?>assets/js/bootstrap-clockpicker.js"></script>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-clockpicker.css"/>

    <script src="<?= base_url() ?>assets/js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-datepicker.css"/>

    <script src="<?= base_url() ?>assets/js/bootstrap-tagsinput.js"></script>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-tagsinput.css"/>

    <!--<script src="<? /*= base_url() */ ?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="<? /*= base_url() */ ?>assets/css/bootstrap-datetimepicker.min.css"/>-->


    <style>
        .vertical-center {
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        .navbar {
            position: relative;
            min-height: 50px;
            margin-bottom: 0px;
            border: 1px solid transparent;
        }

        .bootstrap-tagsinput {
            width: 100% !important;
        }

        .bold {
            font-weight: bold;
        }

        .color-red {
            color: #b90d09
        }

        .color-blue {
            color: #2860ff
        }

        .vertical-align {
            display: flex;
            align-items: center;
        }

        /* The snackbar - position it at the bottom and in the middle of the screen */
        #snackbar {
            visibility: hidden; /* Hidden by default. Visible on click */
            min-width: 250px; /* Set a default minimum width */
            margin-left: -125px; /* Divide value of min-width by 2 */
            background-color: #333; /* Black background color */
            color: #fff; /* White text color */
            text-align: center; /* Centered text */
            border-radius: 2px; /* Rounded borders */
            padding: 16px; /* Padding */
            position: fixed; /* Sit on top of the screen */
            z-index: 10000000000; /* Add a z-index if needed */
            left: 50%; /* Center the snackbar */
            bottom: 30px; /* 30px from the bottom */
        }

        /* Show the snackbar when clicking on a button (class added with JavaScript) */
        #snackbar.show {
            visibility: visible; /* Show the snackbar */
            /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
           However, delay the fade out process for 2.5 seconds */
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        /* Animations to fade the snackbar in and out */
        @-webkit-keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }
            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }
            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }
            to {
                bottom: 0;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }
            to {
                bottom: 0;
                opacity: 0;
            }
        }

        .btn-default {
            width: 100px;
            border-radius: 2px;
        }

        .btn-primary {
            width: 100px;
            border-radius: 2px;
            background-color: #2860ff;
            letter-spacing: 1px;
            text-transform: uppercase;
            background: #2860ff;
            color: #ffffff;
        }

        .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
            background: #2860ff;
        }

        .btn-primary:active, .btn-primary.active {
            background: #2860ff;
            box-shadow: none;
        }

        .btn-success {
            width: 100px;
            border-radius: 2px;
            background-color: #18a800;
            letter-spacing: 1px;
            text-transform: uppercase;
            background: #18a800;
            color: #ffffff;
        }

        .btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .open > .dropdown-toggle.btn-success {
            background: #18a800;
        }

        .btn-success:active, .btn-success.active {
            background: #18a800;
            box-shadow: none;
        }

        @font-face {
            font-family: 'Montserrat';
            font-style: normal;
            src: url(<?=base_url('assets/fonts/Montserrat-Regular.ttf')?>);
        }

        body {
            background: linear-gradient(#0D9FB6, #073D6B);
            font-family: 'Montserrat' !important;
        }

        .vertical-align {
            display: flex;
            align-items: center;
        }

        .navbar-default {
            padding-top: 5px
            min-height: 58px;
        / / background: linear-gradient(#0D9FB6, #073D6B);

            background-color: transparent;
            border-color: transparent;
        }

        .navbar-default .navbar-brand {
            color: #ffffff;
        }

        .navbar-default .navbar-brand:hover,
        .navbar-default .navbar-brand:focus {
            color: #ffffff;
        }

        .navbar-default .navbar-text {
            color: #ffffff;
        }

        .navbar-default .navbar-nav > li > a {
            color: #ffffff;
        }

        .navbar-default .navbar-nav > li > a:hover,
        .navbar-default .navbar-nav > li > a:focus {
            color: #ffffff;
        }

        .navbar-default .navbar-nav > .active > a,
        .navbar-default .navbar-nav > .active > a:hover,
        .navbar-default .navbar-nav > .active > a:focus {
            color: #ffffff;
            background-color: #000000;
        }

        .navbar-default .navbar-nav > .open > a,
        .navbar-default .navbar-nav > .open > a:hover,
        .navbar-default .navbar-nav > .open > a:focus {
            color: #ffffff;
            background-color: #000000;
        }

        .navbar-default .navbar-toggle {
            border-color: #000000;
        }

        .navbar-default .navbar-toggle:hover,
        .navbar-default .navbar-toggle:focus {
            background-color: #000000;
        }

        .navbar-default .navbar-toggle .icon-bar {
            background-color: #ffffff;
        }

        .navbar-default .navbar-collapse,
        .navbar-default .navbar-form {
            border-color: #ffffff;
        }

        .navbar-default .navbar-link {
            color: #ffffff;
        }

        .navbar-default .navbar-link:hover {
            color: #ffffff;
        }

        @media (max-width: 767px) {
            .navbar-default .navbar-nav .open .dropdown-menu > li > a {
                color: #ffffff;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
                color: #ffffff;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
                color: #ffffff;
                background-color: #000000;
            }
        }

        #menu_title {
            margin-left: 5px;
            color: white;
            text-decoration: none
        }

        @font-face {
            font-family: 'Material Icons';
            font-style: normal;
            font-weight: 400;
            src: url(<?=base_url('assets/fonts/MaterialIcons-Regular.eot')?>); /* For IE6-8 */
            src: local('Material Icons'),
            local('MaterialIcons-Regular'),
            url(<?=base_url('assets/fonts/MaterialIcons-Regular.woff2')?>) format('woff2'),
            url(<?=base_url('assets/fonts/MaterialIcons-Regular.woff')?>) format('woff'),
            url(<?=base_url('assets/fonts/MaterialIcons-Regular.ttf')?>) format('truetype');
        }

        .material-icons {
            font-family: 'Material Icons';
            font-weight: normal;
            font-style: normal;
            font-size: 18px; /* Preferred icon size */
            display: inline-block;
            line-height: 1;
            text-transform: none;
            letter-spacing: normal;
            word-wrap: normal;
            white-space: nowrap;
            direction: ltr;

            /* Support for all WebKit browsers. */
            -webkit-font-smoothing: antialiased;
            /* Support for Safari and Chrome. */
            text-rendering: optimizeLegibility;

            /* Support for Firefox. */
            -moz-osx-font-smoothing: grayscale;

            /* Support for IE. */
            font-feature-settings: 'liga';
        }

        /* Rules for sizing the icon. */
        .material-icons.md-18 {
            font-size: 18px;
        }

        .material-icons.md-20 {
            font-size: 20px;
            font-weight: bold;
        }

        .material-icons.md-25 {
            font-size: 25px;
        }

        .material-icons.md-30 {
            font-size: 30px;
        }

        .material-icons.md-40 {
            font-size: 40px;
        }

        .material-icons.md-50 {
            font-size: 50px;
        }

        .material-icons.md-24 {
            font-size: 24px;
        }

        .material-icons.md-36 {
            font-size: 36px;
        }

        .material-icons.md-48 {
            font-size: 48px;
        }

        .material-icons.md-80 {
            font-size: 80px;
        }

        .material-icons.md-100 {
            font-size: 100px;
        }

        /* Rules for using icons as black on a light background. */
        .material-icons.md-dark {
            color: rgba(0, 0, 0, 0.54);
        }

        .material-icons.md-dark.md-inactive {
            color: rgba(0, 0, 0, 0.26);
        }

        /* Rules for using icons as white on a dark background. */
        .material-icons.md-light {
            color: rgba(255, 255, 255, 1);
        }

        .material-icons.md-light.md-inactive {
            color: rgba(255, 255, 255, 0.3);
        }

        .material-icons.md-blue {
            color: rgba(6, 58, 105, 1);
        }

        .material-icons.md-blue.md-inactive {
            color: rgba(255, 255, 255, 0.3);
        }

        /* side bar menu */
        a, a:hover, a:focus {
            color: white;
            text-decoration: none;
            transition: all 0.3s;
        }

        table a:hover, a:focus {
            color: #F8A406;
            text-decoration: none;
            transition: all 0.3s;
        }

        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }

        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */
        .wrapper {
            /* margin-top: z0px; */
            display: flex;
            align-items: stretch;
        }

        #sidebar {
            min-width: 260px;
            max-width: 260px;
        / / background: #ffffff;
        / / color: #063a69;
        / / background: linear-gradient(#0D9FB6, #073D6B);
            transition: all 0.3s;
        }

        #sidebar.active {
            margin-left: -260px;
        }

        #sidebar ul.components {
            padding: 20px 0;
            /*border-bottom: 1px solid #757575;*/
        }

        #sidebar ul p {
            color: #757575;
            padding: 10px;
        }

        #sidebar ul li a {
            font-weight: none;
            padding: 10px;
            font-size: 12px;
            display: block;
        }

        #sidebar ul li a:hover {
            color: #f2f2f2;
            background: linear-gradient(to right, #0D9FB6, #ffffff);
        }

        #sidebar ul li.active > a, a[aria-expanded="true"] {
            color: #757575;
            background: #f2f2f2;
        }

        a[data-toggle="collapse"] {
            position: relative;

        }

        a[aria-expanded="false"]::before, a[aria-expanded="true"]::before {
            content: '\e259';
            display: block;
            position: absolute;
            right: 20px;
            font-family: 'Glyphicons Halflings';
            font-size: 0.6em;
        }

        a[aria-expanded="true"]::before {
            content: '\e260';
        }

        #sidebar ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: transparent;
        }

        /* ---------------------------------------------------
            CONTENT STYLE
        ----------------------------------------------------- */
        #content {
            background-color: white;
            padding: 20px;
            width: 100%;
            min-height: 100vh;
            transition: all 0.3s;

        }

        /* ---------------------------------------------------
            MEDIAQUERIES
        ----------------------------------------------------- */
        @media (max-width: 768px) {
            #sidebar {
                margin-left: -260px;
            }

            #sidebar.active {
                margin-left: 0;
            }

            #sidebarCollapse span {
                display: none;
            }
        }

        .top5 {
            margin-top: 5px;
        }

        .top7 {
            margin-top: 7px;
        }

        .top10 {
            margin-top: 10px;
        }

        .top15 {
            margin-top: 15px;
        }

        .top20 {
            margin-top: 25px;
        }

        .top30 {
            margin-top: 30px;
        }

        .modal-content {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        table {
            font-size: 12px;
        }

        #custom-search-input {
            padding: 3px;
            border: solid 1px #E4E4E4;
            background-color: #fff;
        }

        #custom-search-input input {
            border: 0;
            box-shadow: none;
        }

        #custom-search-input button {
            margin: 2px 0 0 0;
            background: none;
            box-shadow: none;
            border: 0;
            color: #666666;
            padding: 0 8px 0 10px;
            border-left: solid 1px #ccc;
        }

        #custom-search-input button:hover {
            border: 0;
            box-shadow: none;
            border-left: solid 1px #ccc;
        }

        #custom-search-input .glyphicon-search {
            font-size: 14px;
        }

        .link-blue {
            font-weight: bold;
            color: #2860ff;
            text-decoration: none;
            cursor: pointer;
        }

        .autocomplete-wrapper {
            z-index: 10001;
            margin: 44px auto 44px;
            max-width: 600px;
        }

        .autocomplete-wrapper label {
            z-index: 10001;
            display: block;
            margin-bottom: .75em;
            color: #3f4e5e;
            font-size: 1.25em;
        }

        .autocomplete-wrapper .text-field {
            z-index: 10001;
            padding: 0 15px;
            width: 100%;
            height: 40px;
            border: 1px solid #CBD3DD;
            font-size: 1.125em;
        }

        .autocomplete-wrapper ::-webkit-input-placeholder {
            z-index: 10001;
            color: #CBD3DD;
            font-style: italic;
            font-size: 18px;
        }

        .autocomplete-wrapper :-moz-placeholder {
            color: #CBD3DD;
            font-style: italic;
            font-size: 18px;
        }

        .autocomplete-wrapper ::-moz-placeholder {
            color: #CBD3DD;
            font-style: italic;
            font-size: 18px;
        }

        .autocomplete-wrapper :-ms-input-placeholder {
            color: #CBD3DD;
            font-style: italic;
            font-size: 18px;
        }

        .autocomplete-suggestions {
            z-index: 10001;
            overflow: auto;
            border: 1px solid #CBD3DD;
            background: #FFF;
        }

        .autocomplete-suggestion {
            z-index: 10001;
            overflow: hidden;
            padding: 5px 15px;
            white-space: nowrap;
        }

        .autocomplete-selected {
            background: #F0F0F0;
        }

        .autocomplete-suggestions strong {
            color: #029cca;
            font-weight: normal;
        }

        body {
        / / background background-color: #029cca;

        }

        table {
            font-size: 12px;
        }

        #custom-search-input {
            padding: 3px;
            border: solid 1px #E4E4E4;
            background-color: #fff;
        }

        #custom-search-input input {
            border: 0;
            box-shadow: none;
        }

        #custom-search-input button {
            margin: 2px 0 0 0;
            background: none;
            box-shadow: none;
            border: 0;
            color: #666666;
            padding: 0 8px 0 10px;
            border-left: solid 1px #ccc;
        }

        #custom-search-input button:hover {
            border: 0;
            box-shadow: none;
            border-left: solid 1px #ccc;
        }

        #custom-search-input .glyphicon-search {
            font-size: 14px;
        }

        .link-blue {
            font-weight: bold;
            color: #2860ff;
            text-decoration: none;
            cursor: pointer;
        }

        table thead {
            color: #063a69;
        }

        /* .table td {
             background-color: white !important;
         }*/

        .modal-dialog-lg {
            width: 90%;
            height: auto;
            padding: 0;
        }

        .unview {
            background-color: #F8A406 !important;
            color: white !important;
        }

        .white {
            background-color: white !important;
        }

        /*.modal-content {
            height: 100%;
            border-radius: 0;
        }*/

        .navbar-default .dropdown-menu.notify-drop {
            min-width: 330px;
            background-color: #fff;
            min-height: 360px;
            max-height: 360px;
        }

        .navbar-default .dropdown-menu.notify-drop .notify-drop-title {
            border-bottom: 1px solid #e2e2e2;
            padding: 5px 15px 10px 15px;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content {
            min-height: 280px;
            max-height: 280px;
            overflow-y: scroll;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar-track {
            background-color: #F5F5F5;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar {
            width: 8px;
            background-color: #F5F5F5;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar-thumb {
            background-color: #ccc;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li {
            border-bottom: 1px solid #e2e2e2;
            padding: 10px 0px 5px 0px;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li:nth-child(2n+0) {
            background-color: #fafafa;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li:after {
            content: "";
            clear: both;
            display: block;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li:hover {
            background-color: #fcfcfc;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li:last-child {
            border-bottom: none;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li .notify-img {
            float: left;
            display: inline-block;
            width: 45px;
            height: 45px;
            margin: 0px 0px 8px 0px;
        }

        .navbar-default .dropdown-menu.notify-drop .allRead {
            margin-right: 7px;
        }

        .navbar-default .dropdown-menu.notify-drop .rIcon {
            float: right;
            color: #999;
        }

        .navbar-default .dropdown-menu.notify-drop .rIcon:hover {
            color: #333;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li a {
            font-size: 12px;
            font-weight: normal;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li {
            font-weight: bold;
            font-size: 11px;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li hr {
            margin: 5px 0;
            width: 70%;
            border-color: #e2e2e2;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content .pd-l0 {
            padding-left: 0;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li p {
            font-size: 11px;
            color: #666;
            font-weight: normal;
            margin: 3px 0;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li p.time {
            font-size: 10px;
            font-weight: 600;
            top: -6px;
            margin: 8px 0px 0px 0px;
            padding: 0px 3px;
            border: 1px solid #e2e2e2;
            position: relative;
            background-image: linear-gradient(#fff, #f2f2f2);
            display: inline-block;
            border-radius: 2px;
            color: #B97745;
        }

        .navbar-default .dropdown-menu.notify-drop .drop-content > li p.time:hover {
            background-image: linear-gradient(#fff, #fff);
        }

        .navbar-default .dropdown-menu.notify-drop .notify-drop-footer {
            border-top: 1px solid #e2e2e2;
            bottom: 0;
            position: relative;
            padding: 8px 15px;
        }

        .navbar-default .dropdown-menu.notify-drop .notify-drop-footer a {
            color: #777;
            text-decoration: none;
        }

        .navbar-default .dropdown-menu.notify-drop .notify-drop-footer a:hover {
            color: #333;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-default ">
    <div class="container-fluid" style="margin-left: -5px">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img style="width: 40px; height: 44px;margin: 5px" src="<?= base_url() ?>assets/img/logo.png">
        </div>
        <div id="navbar2" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="badge badge-notify" id="totalUnReadSurat"
                              style="margin-left: -20px;background-color: #F8A406"></span>
                        <span style="font-size:20px;color: white" class="glyphicon glyphicon-bell"></span>
                    </a>
                    <ul class="dropdown-menu notify-drop">
                        <!--<div class="drop-content" id="notification-dropdown-menu">
                        </div>-->

                        <div class="drop-content" id="notification-dropdown-menu">
                        </div>

                        <div class="notify-drop-footer text-center">
                            <a href="<?=base_url('notification')?>"><i class="fa fa-eye"></i> Show All</a>
                        </div>
                    </ul>

                    <!--<ul class="dropdown-menu" style="width: 300px; padding: 5px" id="notification-dropdown-menu">
                    </ul>-->
                </li>

                <li>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                </li>
                <li>
                    <?
                    if (getSessionProfilePic() != null) {
                        ?>
                        <img class="img-circle img-responsive " style="width: : 40px; height: 40px;margin-top: 8px"
                             src="<?= base_url() ?>data/<?= getSessionProfilePic() ?>">
                        <?
                    } else {
                        ?>
                        <img class="img-circle img-responsive " style="width: : 40px; height: 40px;margin-top: 8px"
                             src="<?= base_url() ?>assets/img/person.jpg">
                        <?
                    }
                    ?>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">
                        <?= getSessionNama() ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= base_url() ?>user/profile/<?= getSessionUserId() ?>">My Profile</a></li>
                        <li><a href="<?= base_url('/login/logout') ?>">Logout</a></li>
                        <!--<li class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">One more separated link</a></li>-->
                    </ul>
                </li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</nav>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <ul class="list-unstyled components">
            <?
            if (getSessionRole() == 'ADMIN' || getSessionRole() == 'SUPER ADMIN') {
                ?>
                <li>
                    <a href="#userDashboard" data-toggle="collapse" aria-expanded="false">USER</a>
                    <ul class="collapse list-unstyled" id="userDashboard">
                        <li><a href="<?= base_url('/admin/form') ?>">TAMBAH USER</a></li>
                        <li><a href="<?= base_url('/admin') ?>">DAFTAR USER</a></li>
                    </ul>
                </li>

                <?
            }
            ?>

            <?
            if (getSessionRole() == 'REQUESTER') {
                ?>

                <li>
                    <a href="<?= base_url('/dashboard/requester') ?>">DASHBOARD</a>
                </li>

                <li>
                    <a href="<?= base_url('/requestsurat') ?>">REQUEST SURAT</a>
                </li>

                <li>
                    <a href="<?= base_url('/requestsurat/requester') ?>">DATA REQUEST SURAT</a>
                </li>

                <?
            }
            ?>

            <?
            if (getSessionRole() == 'ADMIN' || getSessionRole() == 'SUPER ADMIN') {
                ?>

                <li>
                    <a href="<?= base_url('/dashboard/admin') ?>">DASHBOARD</a>
                </li>

                <li>
                    <a href="<?= base_url('/requestsurat/admin') ?>">DATA REQUEST SURAT</a>
                </li>

                <?
            }
            ?>

            <?
            if (getSessionRole() == 'STAFF') {
                ?>

                <li>
                    <a href="<?= base_url('/dashboard/staff') ?>">DASHBOARD</a>
                </li>

                <li>
                    <a href="<?= base_url('/requestsurat/staff') ?>">DATA REQUEST SURAT</a>
                </li>

                <?
            }
            ?>

            <?
            if (getSessionRole() == 'SUPER ADMIN') {
                ?>

                <li>
                    <a href="<?= base_url('/report') ?>">REPORT KPI</a>
                </li>

                <?
            }
            ?>


        </ul>
    </nav>

    <div id="snackbar">Some text some message..</div>

    <!-- Page Content Holder -->
    <br>
    <br>
    <div id="content">
