<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    var $title = "DASHBOARD";
    var $cUri = "dashboard";
    var $menuName = "Dashboard";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('RequestSuratModel');
    }

    public function getTotalSummaryRequest()
    {

        $user = new UserModel();
        $userList = $user->getUserByRole('STAFF');

        $summaryList = array();

        foreach ($userList as $user) {
            $tmp = array();
            $tmp['fullname'] = $user->nama_lengkap;

            $surat = new RequestSuratModel();
            $tmp['totalApproved'] = $surat->getTotalRequest($user->user_id, 'APPROVED');
            $tmp['totalRejected'] = $surat->getTotalRequest($user->user_id, 'REJECTED');
            $tmp['totalPending'] = $surat->getTotalRequest($user->user_id, 'PENDING');

            array_push($summaryList, $tmp);

        }

        printJson($this, $summaryList);
    }

    public function admin()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $surat = new RequestSuratModel();
        $query = "select request_surat.*, user.nama_lengkap from request_surat
                    inner join user on request_surat.created_by = user.user_id
                    where tanggal_surat = '" . nowDate() . "' limit 5";
        $data['todaySuratList'] = $surat->dbGetRows($query);

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat";
        $data['totalSurat'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat where status = 'ASSIGNED'";
        $data['totalAssigned'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat where status = 'APPROVED'";
        $data['totalApproved'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat where status = 'REQUESTED'";
        $data['totalRequested'] = $surat->dbGetRow($query)->total;

        $this->load->view('HeaderView', $data);
        $this->load->view('DashboardAdminView', $data);
        $this->load->view('FooterView', $data);
    }

    public function requester()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Dashboard Requster';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat where created_by = " . getSessionUserId();
        $data['totalSurat'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat where status = 'ASSIGNED' and created_by = " . getSessionUserId();
        $data['totalAssigned'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat where status = 'APPROVED' and created_by = " . getSessionUserId();
        $data['totalApproved'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat where status = 'REQUESTED' and created_by = " . getSessionUserId();
        $data['totalRequested'] = $surat->dbGetRow($query)->total;

        $this->load->view('HeaderView', $data);
        $this->load->view('DashboardRequesterView', $data);
        $this->load->view('FooterView', $data);
    }

    public function staff()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Dashboard Staff';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat
                    inner join request_surat_assign on request_surat_assign.request_surat_id = request_surat.request_surat_id
                    inner join user on user.user_id = request_surat_assign.user_id
                    where request_surat_assign.user_id = " . getSessionUserId();
        $data['totalSurat'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat
                    inner join request_surat_assign on request_surat_assign.request_surat_id = request_surat.request_surat_id
                    inner join user on user.user_id = request_surat_assign.user_id
                    where status = 'ASSIGNED' and request_surat_assign.user_id = " . getSessionUserId();
        $data['totalAssigned'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat
                  inner join request_surat_assign on request_surat_assign.request_surat_id = request_surat.request_surat_id
                  inner join user on user.user_id = request_surat_assign.user_id
                  where status = 'APPROVED' and request_surat_assign.user_id = " . getSessionUserId();
        $data['totalApproved'] = $surat->dbGetRow($query)->total;

        $surat = new RequestSuratModel();
        $query = "select count(*) as total from request_surat
                  inner join request_surat_assign on request_surat_assign.request_surat_id = request_surat.request_surat_id
                  inner join user on user.user_id = request_surat_assign.user_id
                  where status = 'REQUESTED' and request_surat_assign.user_id = " . getSessionUserId();
        $data['totalRequested'] = $surat->dbGetRow($query)->total;

        $this->load->view('HeaderView', $data);
        $this->load->view('DashboardStaffView', $data);
        $this->load->view('FooterView', $data);
    }
}