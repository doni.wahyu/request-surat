<br>
<div class="row">
    <div class="col-md-5">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" name="search_query" id="search_query" class="form-control" placeholder="Customer Name"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
            </div>
        </div>
    </div>
</div>

<div id="modal_detail" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="height: auto;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <br>
                <p style="font-size: 18px;color: #757575;font-weight: bold;">Customer Name : <a style="color: #2860ff"
                                                                                               id="nama_anggota"></a>
                </p>

                <input type="text" class="hidden" id="user_id" name="user_id">

                <p>Status : <a style="font-size: 12px;font-weight: bold" id="status"></a>
                    <button id="btnReactive" type="button" class="btn btn-success" onclick="reactive()"
                            style="display: none">Re-active
                    </button>
                </p>
                <p id="member_sejak" style="font-size: 12px;color: #757575"></p>
                <p id="address" style="font-size: 12px;color: #757575"></p>
                <p id="phone_number" style="font-size: 12px;color: #757575"></p>
                <p id="email" style="font-size: 12px;color: #757575"></p>
                <p id="note" style="font-size: 12px;color: #757575"></p>
                <p id="member_sejak" style="font-size: 12px;color: #757575"></p>

                <br>
                <p id="member_sejak" style="font-size: 14px;color: #4f4f4f;font-weight: bold;">EMERGENCY CONTACT</p>

                <p id="emergency_contact" style="font-size: 12px;color: #757575"></p>
                <p id="emergency_phone_number" style="font-size: 12px;color: #757575"></p>
                <p id="note" style="font-size: 12px;color: #757575"></p>

                <br>
                <p id="member_sejak" style="font-size: 14px;color: #4f4f4f;font-weight: bold;">MEMBERSHIP INFORMATION</p>

                <p id="member_type" style="font-size: 12px;color: #757575"></p>
                <p id="total_quota" style="font-size: 12px;color: #757575"></p>
                <p id="day_quota" style="font-size: 12px;color: #757575"></p>

                <br>
                <button type="button" class="btn btn-primary" onclick="doEdit()">EDIT</button>
            </div>
        </div>
    </div>
</div>

<br>
<div id="response">
    <table id="table-user" class="table table-striped">
        <thead>
        <tr style="background-color: #f2f2f2">
            <th>Customer Name</th>
            <th>Membership Type</th>
            <th>Status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<input type="hidden" id="pageno" value="0">
<img id="loader" src="<?= base_url() ?>/assets/img/loader.svg">

<script>
    $(document).ready(function () {
        searchData();

        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                searchData();
            }
        });


        $('#search_query').keypress(function (e) {
            if (e.which == 13) {
                resetAndSearchData();
            }
        });

    });

    function resetAndSearchData() {
        $('#pageno').val("0");
        $("#table-user").find("tr:gt(0)").remove();
        searchData();
    }

    function searchData() {
        var nextPage = parseInt($('#pageno').val()) + 1;
        $.ajax({
            type: 'GET',
            url: '<?= $cUri ?>/getAll',
            data: {
                pageno: nextPage, search_query: $('#search_query').val()
            },
            success: function (result) {
                if (result.data.length > 0) {

                    $i = 0;
                    $.each(result.data, function () {
                        var color = ($i % 2 == 0) ? '' : 'active';

                        var statusUser = (this.is_active == 1) ? 'ACTIVE' : 'SUSPEND';
                        var classColor = (this.is_active == 1) ? '' : 'color-red';
                        var flag = (this.member_type_id != null) ? ' <img src="<?=base_url()?>assets/img/red_flag.png" style="width:12px;height:12px">' : '';
                        var memberType = (this.member_type_name == null) ? '' : this.member_type_name;

                        $html = '<tr class="' + color + '"><td>' + this.fullname + '</td><td>' + memberType + flag + '</td><td class="bold ' + classColor + '">'
                            + statusUser + '</td><td><a class="link-blue" href="#" onclick="detailUser(' + this.user_id + ')">DETAIL</a></td></tr>';

                        $('#table-user tr:last').after($html);
                        $('#pageno').val(nextPage);

                        $i++;
                    });
                }

                $("#loader").hide();
            }
        });
    }

    function detailUser(userId) {
        $.getJSON('<?=$cUri?>/getById/' + userId, function (result) {
            $('#modal_detail').modal('show');
            $('#user_id').val(result.user_id);
            $('#status').html('' + (result.is_active == 1 ? 'ACTIVE' : 'SUSPEND'));
            var classColor = (result.is_active == 1) ? '' : 'color-red';
            $("#status").attr('class', classColor);

            if (result.is_active == 0) {
                $('#btnReactive').show();
            } else {
                $('#btnReactive').hide();
            }

            $('#nama_anggota').html(result.fullname);
            $('#member_sejak').html('Membership Date : ' + result.created_date);
            $('#phone_number').html('Phone Number : ' + result.phone_number);
            $('#note').html('Note :' + (result.note == null ? '-' : result.note));
            $('#emergency_contact').html('Contact Name : ' + (result.emergency_contact == null ? '-' : result.emergency_contact));
            $('#emergency_phone_number').html('Phone Number : ' + (result.emergency_phone_number == null ? '-' : result.emergency_phone_number));
            $('#member_type').html('Membership Type : ' + (result.member_type_name == null ? '-' : result.member_type_name));
            $('#total_quota').html('Remaining Quota : ' + (result.total_quota == null ? '-' : (result.total_quota == -1? 'UNLIMITED' : result.total_quota)));
            $('#day_quota').html('Active Period : ' + (result.day_quota == null ? '-' : result.day_quota));
        });
    }

    function doEdit() {
        window.location = '<?=base_url($this->cUri)?>/form/' + $('#user_id').val();
    }

    function reactive() {
        bootbox.confirm("Do you want to Re-active (" + $('#nama_anggota').text() + ")?", function (result) {
            if (result) {
                $.ajax({
                    type: 'GET', url: '<?=$cUri?>/reactive/' + $('#user_id').val(),
                    success: function (data) {
                        bootbox.alert(data.message, function () {
                            window.location.reload();
                        });
                        $.unblockUI();
                    },
                    error: function (response) {
                        message(response.responseText);
                        $.unblockUI();
                    }
                });

            }
        });
    }

</script>