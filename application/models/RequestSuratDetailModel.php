<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class RequestSuratDetailModel extends BaseModel
{
    function RequestSuratDetailModel()
    {
        parent::__construct();
        $this->tableName = 'request_surat_detail';
        $this->primaryKeyName = 'request_surat_detail_id';
    }

    function getByRequestSuratId($key, $lastRevNo = NULL)
    {
        $this->db->where('request_surat_id', $key);

        if ($lastRevNo != NULL) {
            $this->db->where('revision_no', $lastRevNo);
        }

        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    function getLastRevNo($request_surat_id)
    {
        $this->db->select_max('revision_no');
        $this->db->where('request_surat_id', $request_surat_id);
        $query = $this->db->get($this->tableName);
        $result = $query->row_array();
        return $result['revision_no'];
    }
}