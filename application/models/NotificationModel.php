<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class NotificationModel extends BaseModel
{
    function NotificationModel()
    {
        parent::__construct();
        $this->tableName = 'notification';
        $this->primaryKeyName = 'notification_id';
    }

    function getByUser($user_id, $is_viewed = NULL)
    {
        $this->db->where('user_id', $user_id);
        if ($is_viewed != NULL) {
            $this->db->where('user_id', $user_id);
        }
        $this->db->order_by('notification_id', 'desc');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
}