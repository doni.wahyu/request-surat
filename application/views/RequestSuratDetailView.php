<p style="color: #063a69;font-size: 16px;font-weight: bold;">Request Surat</p>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
            <tr>

            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Jenis Surat</th>
                <td><?= $jenisSurat->jenis_name ?></td>
            </tr>
            <tr>
                <th scope="row">Nomor Surat</th>
                <td><?= $requestSurat->no_surat ?></td>
            </tr>
            <tr>
                <th scope="row">Judul Surat</th>
                <td><?= $requestSurat->judul_surat ?></td>
            </tr>
            <tr>
                <th scope="row">Nota Dinas</th>
                <td><?= $requestSurat->nota_dinas ?></td>
            </tr>
            <tr>
                <th scope="row">Status</th>
                <td><?= $requestSurat->status ?></td>
            </tr>

            </tbody>
        </table>
    </div>
    <div class="col-md-4">

    </div>

</div>

<form id="data" role="form" method="post">

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="mydata">
                <thead class="bg-primary">
                <tr>
                    <th scope="col">Detail</th>
                    <th scope="col">Isi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($requestSuratDetailList as $row) {
                    ?>
                    <tr>
                        <td><?= strtoupper($row->type_detail) ?></td>
                        <td style="text-align: justify"><?= $row->isi ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead class="bg-primary">
                <tr>
                    <th scope="col">Lampiran</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($attachmentList as $attachment) {
                    ?>
                    <tr>
                        <td>
                            <a class="link-blue" target="_blank" href="<?= base_url() . 'data/' . $attachment->file_attachment ?>"><?= $attachment->file_attachment ?></a>
                        </td>
                    </tr>
                    <?
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>

    <input type="hidden" id="request_surat_id" name="request_surat_id" value="<?= $requestSurat->request_surat_id ?>">
</form>

<?php
if ($requestSurat->status == 'APPROVED' && getSessionRole() != 'REQUESTER') {
    ?>
    <div class="row top20">
        <div class="col-md-2">
            <button type="button" style="width: 100%" class="btn btn-info" onclick="doPrint()">PRINT</button>
        </div>
    </div>
    <?php
}
?>

<?php

if ($requestSurat->status == 'REJECTED') {

    function getWeekdayDifference($startDate, $endDate)
    {
        $datetime1 = new DateTime($startDate);
        $datetime2 = new DateTime($endDate);
        $interval = $datetime1->diff($datetime2);
        $woweekends = 0;
        for ($i = 0; $i <= $interval->d; $i++) {
            $datetime1->modify('+1 day');
            $weekday = $datetime1->format('w');

            if ($weekday !== "0" && $weekday !== "6") { // 0 for Sunday and 6 for Saturday
                $woweekends++;
            }
        }

        return $woweekends;
    }

    /*$diff = strtotime(date('Y-m-d')) - strtotime($requestSurat->tanggal_surat);
    $totalDateDiff = abs(round($diff / 86400));*/

    $totalDateDiff = getWeekdayDifference($requestSurat->tanggal_surat, date('Y-m-d'));
    if ($totalDateDiff > 2) {
        ?>
        <div class="row top20">
            <div class="col-md-8">
                <div class="alert alert-danger" role="alert">
                    Request Surat sudah lebih dari 3 hari, harap membuat request surat kembali.
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="row top20">
            <div class="col-md-2">
                <button type="button" style="width: 100%" class="btn btn-info" onclick="doRevisi()">REVISI</button>
            </div>
        </div>
        <?php
    }
}
?>

<script>
    function doRevisi() {
        $.blockUI();
        var dataForm = $('#data').serialize();
        $.ajax({
            url: '<?=$cUri?>/doRevisi',
            type: 'POST',
            data: dataForm,
            success: function (data) {
                if (data.success) {
                    bootbox.alert(data.message, function () {
                        location.href = data.redirect;
                    });
                } else {
                    message(data.message)
                }
                $.unblockUI();
            },
            error: function (response) {
                message(response.responseText);
                $.unblockUI();
            }
        });
    }

    function doPrint() {
        window.open('<?=base_url()?>requestsurat/pdf/<?=$requestSurat->request_surat_id?>', '_blank');
    }
</script>
