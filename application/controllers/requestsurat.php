<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestSurat extends MY_Controller
{
    var $title = "Request Surat";
    var $cUri = "requestsurat";
    var $menuName = "Request Surat";

    function __construct()
    {
        parent::__construct();
        $this->load->model('RequestSuratModel');
        $this->load->model('JenisSuratModel');
        $this->load->model('RequestSuratDetailModel');
        $this->load->model('AttachmentModel');
        $this->load->model('UserModel');
        $this->load->model('RequestSuratAssignModel');
    }

    public function index($id = null)
    {
        $action = ($id == null) ? 'Add' : 'Edit';
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = ($id == null) ? 'Request Surat' : 'Request Surat';

        $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
        $data['breadcrumb'] = $breadcrumb;

        $requestsurat = new RequestSuratModel();

        if ($id == null) {
            $data['model'] = null;
        } else {
            $data['model'] = $requestsurat->getById($id);
        }

        $jenisurat = new JenisSuratModel();
        $data['jenissuratList'] = $jenisurat->dbGetAll();

        $this->load->view('HeaderView', $data);
        $this->load->view('RequestSuratFormView', $data);
        $this->load->view('FooterView', $data);
    }

    public function save()
    {
        $id = $this->input->post('request_surat_id');
        $no_surat = $this->input->post('no_surat');
        $judul_surat = $this->input->post('judul_surat');

        $requestsurat = new RequestSuratModel();
        $requestsurat->jenis_surat_id = $this->input->post('jenis_surat_id');
        $requestsurat->no_surat = $no_surat;
        $requestsurat->judul_surat = $judul_surat;
        $requestsurat->nota_dinas = $this->input->post('nota_dinas');
        $requestsurat->tanggal_surat = $this->input->post('tanggal_surat');
        $menimbang = $this->input->post('menimbang');
        $mengingat = $this->input->post('mengingat');
        $memperhatikan = $this->input->post('memperhatikan');
        $memutuskan = $this->input->post('memutuskan');
        $file_attachment = $this->input->post('file_attachment');
        $requestsurat->revision_no = 0;
        $requestsurat->status = 'REQUESTED';

        if ($id) {
            $requestsurat->updated_by = getSessionUserId();
            $requestsurat->updated_date = now();
        } else {
            $requestsurat->created_by = getSessionUserId();
            $requestsurat->created_date = now();
        }

        if ($id) {
            if ($requestsurat->dbUpdate($requestsurat->fetch(), $id)) {
                $res = array('success' => true, 'message' => $this->menuName . ' updated', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => $this->menuName . ' fail update');
            }
        } else {
            if ($requestsurat->dbInsert($requestsurat->fetch())) {
                $newRequestSuratId = $requestsurat->getLastcreatedId();

                foreach ($menimbang as $item) {
                    $requestSuratDetail = new RequestSuratDetailModel();
                    $requestSuratDetail->request_surat_id = $newRequestSuratId;
                    $requestSuratDetail->type_detail = 'menimbang';
                    $requestSuratDetail->isi = $item;
                    $requestSuratDetail->revision_no = 0;
                    $requestSuratDetail->created_by = getSessionUserId();
                    $requestSuratDetail->created_date = now();
                    $requestSuratDetail->dbInsert($requestSuratDetail->fetch());
                }

                foreach ($mengingat as $item) {
                    $requestSuratDetail = new RequestSuratDetailModel();
                    $requestSuratDetail->request_surat_id = $newRequestSuratId;
                    $requestSuratDetail->type_detail = 'mengingat';
                    $requestSuratDetail->isi = $item;
                    $requestSuratDetail->revision_no = 0;
                    $requestSuratDetail->created_by = getSessionUserId();
                    $requestSuratDetail->created_date = now();
                    $requestSuratDetail->dbInsert($requestSuratDetail->fetch());
                }

                foreach ($memperhatikan as $item) {
                    $requestSuratDetail = new RequestSuratDetailModel();
                    $requestSuratDetail->request_surat_id = $newRequestSuratId;
                    $requestSuratDetail->type_detail = 'memperhatikan';
                    $requestSuratDetail->isi = $item;
                    $requestSuratDetail->revision_no = 0;
                    $requestSuratDetail->created_by = getSessionUserId();
                    $requestSuratDetail->created_date = now();
                    $requestSuratDetail->dbInsert($requestSuratDetail->fetch());
                }

                foreach ($memutuskan as $item) {
                    $requestSuratDetail = new RequestSuratDetailModel();
                    $requestSuratDetail->request_surat_id = $newRequestSuratId;
                    $requestSuratDetail->type_detail = 'memutuskan';
                    $requestSuratDetail->isi = $item;
                    $requestSuratDetail->revision_no = 0;
                    $requestSuratDetail->created_by = getSessionUserId();
                    $requestSuratDetail->created_date = now();
                    $requestSuratDetail->dbInsert($requestSuratDetail->fetch());
                }

                foreach ($file_attachment as $item) {
                    if ($item != '') {
                        $Attachment = new AttachmentModel();
                        $Attachment->request_surat_id = $newRequestSuratId;
                        $Attachment->file_attachment = $item;
                        $Attachment->created_by = getSessionUserId();
                        $Attachment->created_date = now();
                        $Attachment->dbInsert($Attachment->fetch());
                    }
                }

                $userNotification = new UserModel();
                $userNotificationList = $userNotification->getUserByRole('ADMIN');
                foreach ($userNotificationList as $user) {
                    $message = "Surat No $no_surat ($judul_surat) telah dibuat oleh " . getSessionNama() . ", mohon segera diproses";
                    insertNotification($newRequestSuratId, $user->user_id, $message, getSessionUserId());
                }

                $res = array('success' => true, 'message' => $this->menuName . ' berhasil disubmit', 'redirect' => base_url('requestsurat/requester'));
            } else {
                $res = array('success' => false, 'message' => $this->menuName . ' gagal disubmit');
            }
        }

        printJson($this, $res);
    }

    public function pdf($id)
    {
        include_once APPPATH . '/third_party/mpdf/mpdf.php';
        include_once APPPATH . '/third_party/phpqrcode/qrlib.php';

        if ($id) {
            $data['title'] = 'Data Request Surat';

            $surat = new RequestSuratModel();
            $surat = $surat->getById($id);
            $data['surat'] = $surat;

            $qrImage = 'qr_code_' . $id . '.png';
            $path = './data/' . $qrImage;
            QRcode::png($surat->no_surat, $path);

            $suratDetail = new RequestSuratDetailModel();
            $data['suratDetailList'] = $suratDetail->getByRequestSuratId($id);
            $data['qrCodePath'] = $path;

            $this->load->view('ReportSuratView', $data);

            /*if ($surat->pdf_file == NULL) {
                $qrImage = 'qr_code_' . $id . '.png';
                $path = './data/' . $qrImage;
                QRcode::png($surat->no_surat, $path);

                $suratDetail = new RequestSuratDetailModel();
                $data['suratDetailList'] = $suratDetail->getByRequestSuratId($id);
                $data['qrCodePath'] = $path;

//            $this->load->view('ReportSuratView', $data);

                $html = $this->load->view('ReportSuratView', $data, true);
                $mPDF = new mPDF('"en-GB-x","A4","","",5,5,5,5,0,0');
                $mPDF->WriteHTML($html);

                $filename = str_replace("/", "_", $surat->no_surat) . '.pdf';
                $mPDF->Output('./data/' . $filename, 'F');

                $generatePdf = new RequestSuratModel();
                $generatePdf->pdf_file = $filename;
                $generatePdf->dbUpdate($generatePdf->fetch(), $id);

                redirect(base_url('data') . '/' . $filename);
            } else {
                redirect(base_url('data') . '/' . $surat->pdf_file);
            }*/
        }
    }

    public function requester()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Data Request Surat';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $jenissurat = new JenisSuratModel();
        $data['jenissuratList'] = $jenissurat->dbGetAll();

        $this->load->view('HeaderView', $data);
        $this->load->view('RequesterRequestView', $data);
        $this->load->view('FooterView', $data);
    }

    public function staff()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Data Request Surat';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $jenissurat = new JenisSuratModel();
        $data['jenissuratList'] = $jenissurat->dbGetAll();

        $this->load->view('HeaderView', $data);
        $this->load->view('StaffRequestView', $data);
        $this->load->view('FooterView', $data);
    }

    public function admin()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Data Request Surat';

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $jenissurat = new JenisSuratModel();
        $data['jenissuratList'] = $jenissurat->dbGetAll();

        $this->load->view('HeaderView', $data);
        $this->load->view('AdminRequestView', $data);
        $this->load->view('FooterView', $data);
    }

    public function detail($id)
    {
        if ($id) {
            $action = ($id == null) ? 'Add' : 'Edit';
            $data['cUri'] = base_url($this->cUri);
            $data['title'] = 'Detail Surat';

            $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
            $data['breadcrumb'] = $breadcrumb;

            $requestsurat = new RequestSuratModel();

            if ($id == null) {
                $data['model'] = null;
            } else {
                $requestsurat = $requestsurat->getById($id);
                $data['requestSurat'] = $requestsurat;

                $jenisSuratExist = new JenisSuratModel();
                $data['jenisSurat'] = $jenisSuratExist->getById($requestsurat->jenis_surat_id);

                $attachmentModel = new AttachmentModel();
                $data['attachmentList'] = $attachmentModel->getByRequestSurat($id);

                if ($requestsurat->status == 'APPROVED' && $requestsurat->created_by == getSessionUserId()) {
                    markReadNotificationPrev($id);
                }
            }

            //
            $jenisurat = new JenisSuratModel();
            $data['jenissuratList'] = $jenisurat->dbGetAll('jenis_name');

            $requestSuratDetail = new RequestSuratDetailModel();
            $data['requestSuratDetailList'] = $requestSuratDetail->getByRequestSuratId($id);

            $userStaff = new UserModel();
            $data['staffList'] = $userStaff->dbGetRows("select * from user where role = 'STAFF'");


            $this->load->view('HeaderView', $data);
            $this->load->view('RequestSuratDetailView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    public function editform($id = null)
    {
        if ($id) {
            $action = ($id == null) ? 'Add' : 'Edit';
            $data['cUri'] = base_url($this->cUri);
            $data['title'] = ($id == null) ? 'Request Surat' : 'Request Surat';

            $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
            $data['breadcrumb'] = $breadcrumb;

            $requestsurat = new RequestSuratModel();

            if ($id == null) {
                $data['model'] = null;
            } else {
                $data['model'] = $requestsurat->getById($id);
            }

            $jenisurat = new JenisSuratModel();
            $data['jenissuratList'] = $jenisurat->dbGetAll();

            $this->load->view('HeaderView', $data);
            $this->load->view('EditSuratFormView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    public function edit()
    {
        $id = $this->input->post('request_surat_id');

        if ($id) {
            $no_surat = $this->input->post('no_surat');
            $judul_surat = $this->input->post('judul_surat');

            $requestsurat = new RequestSuratModel();
            $requestsurat->jenis_surat_id = $this->input->post('jenis_surat_id');
            $requestsurat->no_surat = $no_surat;
            $requestsurat->judul_surat = $judul_surat;
            $requestsurat->nota_dinas = $this->input->post('nota_dinas');
            $requestsurat->tanggal_surat = $this->input->post('tanggal_surat');
            $requestsurat->updated_by = getSessionUserId();
            $requestsurat->updated_date = now();

            if ($requestsurat->dbUpdate($requestsurat->fetch(), $id)) {
                $res = array('success' => true, 'message' => $this->menuName . ' updated', 'redirect' => base_url('requestsurat/admin'));
            } else {
                $res = array('success' => false, 'message' => $this->menuName . ' fail update');
            }
        } else {
            $res = array('success' => false, 'message' => $this->menuName . ' fail update');
        }

        printJson($this, $res);
    }

    public function getAll()
    {
        $pageno = $_GET['pageno'];
        $search_query = $_GET['search_query'];
        $tanggal_surat = $_GET['tanggal_surat'];
        $jenis_surat_id = $_GET['jenis_surat_id'];
        $status = $_GET['status'];

        $no_of_records_per_page = 50;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $holiday = new RequestSuratModel();


        $query = "SELECT request_surat.*, jenis_name FROM request_surat INNER JOIN jenis_surat on jenis_surat.jenis_surat_id = request_surat.jenis_surat_id WHERE 1 = 1";

        if ($search_query != '') {
            $query .= " AND (no_surat LIKE '%$search_query%' OR judul_surat LIKE '%$search_query%'
                OR tanggal_surat LIKE '%$search_query%'
                OR request_surat.status LIKE '%$search_query%')";
        }

        if ($tanggal_surat <> '') {
            $query .= " AND tanggal_surat = '$tanggal_surat'";
        }

        if ($jenis_surat_id <> '') {
            $query .= " AND request_surat.jenis_surat_id = $jenis_surat_id";
        }

        if ($status <> '') {
            $query .= " AND request_surat.status = '$status'";
        }

        $query .= " order by request_surat.request_surat_id desc LIMIT $offset, $no_of_records_per_page";

        $data = $holiday->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }

    public function verifikasi($id = null)
    {
        if ($id) {
            $action = ($id == null) ? 'Add' : 'Edit';
            $data['cUri'] = base_url($this->cUri);
            $data['title'] = 'Detail Surat';

            $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
            $data['breadcrumb'] = $breadcrumb;

            $data['request_surat_id'] = $id;

            $requestsurat = new RequestSuratModel();

            if ($id == null) {
                $data['model'] = null;
            } else {
                $requestsurat = $requestsurat->getById($id);
                $data['requestSurat'] = $requestsurat;

                $jenisSuratExist = new JenisSuratModel();
                $data['jenisSurat'] = $jenisSuratExist->getById($requestsurat->jenis_surat_id);

                $attachmentModel = new AttachmentModel();
                $data['attachmentList'] = $attachmentModel->getByRequestSurat($id);
            }
//
            $jenisurat = new JenisSuratModel();
            $data['jenissuratList'] = $jenisurat->dbGetAll('jenis_name');

            $requestSuratDetailNo = new RequestSuratDetailModel();
            $lastRevNo = $requestSuratDetailNo->getLastRevNo($id);

            $requestSuratDetail = new RequestSuratDetailModel();
            $data['requestSuratDetailList'] = $requestSuratDetail->getByRequestSuratId($id, $lastRevNo);

            $userStaff = new UserModel();
            $data['staffList'] = $userStaff->dbGetRows("select * from user where role = 'STAFF'");

            $this->load->view('HeaderView', $data);
            $this->load->view('VerifikasiFormView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    function submitComment()
    {
        $request_surat_detail_id = $this->input->post('request_surat_detail_id');


        $requestSuratDetail = new RequestSuratDetailModel();
        $requestSuratDetail->comment = (trim($this->input->post('comment')));

        if ($requestSuratDetail->dbUpdate($requestSuratDetail->fetch(), $request_surat_detail_id)) {
            $res = array('success' => true, 'message' => 'Revisi berhasil ditambahakn', 'redirect' => base_url($this->cUri));
        } else {
            $res = array('success' => false, 'message' => 'Revisi gagal ditambahkan');
        }


        printJson($this, $res);
    }

    public function assign($id = null)
    {
        if ($id) {
            $action = ($id == null) ? 'Add' : 'Edit';
            $data['cUri'] = base_url($this->cUri);
            $data['title'] = 'Detail Surat';

            $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
            $data['breadcrumb'] = $breadcrumb;

            $requestsurat = new RequestSuratModel();

            if ($id == null) {
                $data['model'] = null;
            } else {
                $requestsurat = $requestsurat->getById($id);
                $data['requestSurat'] = $requestsurat;

                $jenisSuratExist = new JenisSuratModel();
                $data['jenisSurat'] = $jenisSuratExist->getById($requestsurat->jenis_surat_id);

                $attachmentModel = new AttachmentModel();
                $data['attachmentList'] = $attachmentModel->getByRequestSurat($id);
            }
//
            $jenisurat = new JenisSuratModel();
            $data['jenissuratList'] = $jenisurat->dbGetAll('jenis_name');

            $requestSuratDetail = new RequestSuratDetailModel();
            $data['requestSuratDetailList'] = $requestSuratDetail->getByRequestSuratId($id);

            $userStaff = new UserModel();
            $data['staffList'] = $userStaff->dbGetRows("select * from user where role = 'STAFF'");

            $this->load->view('HeaderView', $data);
            $this->load->view('AssignFormView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    function doAssign()
    {
        $request_surat_id = $this->input->post('request_surat_id');
        $user_id = $this->input->post('user_id');

        $requestsurat = new RequestSuratModel();
        $requestsurat->status = 'ASSIGNED';

        if ($requestsurat->dbUpdate($requestsurat->fetch(), $request_surat_id)) {
            $assignsurat = new RequestSuratAssignModel();
            $assignsurat->request_surat_id = $request_surat_id;
            $assignsurat->user_id = $user_id;
            $assignsurat->assign_date = now();
            $assignsurat->dbInsert($assignsurat->fetch());

            markReadNotificationPrev($request_surat_id);

            $reqSuratExisting = new RequestSuratModel();
            $reqSuratExisting = $reqSuratExisting->getById($request_surat_id);

            $message = "Surat No " . $reqSuratExisting->no_surat . " (" . $reqSuratExisting->judul_surat . ") mohon segera diverifikasi";
            insertNotification($request_surat_id, $user_id, $message, getSessionUserId());

            $res = array('success' => true, 'message' => 'Berhasil di assign', 'redirect' => base_url('requestsurat/admin'));
        } else {
            $res = array('success' => false, 'message' => 'Asign gagal');
        }

        printJson($this, $res);
    }

    function addPoint($request_surat_id, $point)
    {
        $reqSuratPoint = new RequestSuratAssignModel();
        $reqSuratPoint->point = $point;
        $reqSuratPoint->followup_date = now();
        $reqSuratPoint->dbUpdateArry($reqSuratPoint->fetch(), array('request_surat_id' => $request_surat_id, 'user_id' => getSessionUserId()));
    }

    public function doApprove()
    {
        $request_surat_id = $this->input->post('request_surat_id');

        $requestSuratApprove = new RequestSuratModel();
        $requestSuratApprove->status = 'APPROVED';

        if ($requestSuratApprove->dbUpdate($requestSuratApprove->fetch(), $request_surat_id)) {

            markReadNotificationPrev($request_surat_id);

            $reqSuratExisting = new RequestSuratModel();
            $reqSuratExisting = $reqSuratExisting->getById($request_surat_id);

            $message = "Surat No " . $reqSuratExisting->no_surat . " (" . $reqSuratExisting->judul_surat . ") telah disetujui";
            insertNotification($request_surat_id, $reqSuratExisting->created_by, $message, getSessionUserId());

            $this->addPoint($request_surat_id, INCREASE_POINT);

            $res = array('success' => true, 'message' => ' Request Surat berhasil diapprove', 'redirect' => base_url('requestsurat/staff'));
        } else {
            $res = array('success' => false, 'message' => 'Request Surat gagal diapprove');
        }

        printJson($this, $res);
    }

    public function doReject()
    {
        $request_surat_id = $this->input->post('request_surat_id');

        $requestSuratDetail = new RequestSuratDetailModel();
        $requestSuratDetailList = $requestSuratDetail->getByRequestSuratId($request_surat_id);

        $isValid = false;
        foreach ($requestSuratDetailList as $requestSuratDetail) {
            if ($requestSuratDetail->comment != '') {
                $isValid = true;
            }
        }

        if ($isValid) {
            $requestSuratApprove = new RequestSuratModel();
            $requestSuratApprove->status = 'REJECTED';

            if ($requestSuratApprove->dbUpdate($requestSuratApprove->fetch(), $request_surat_id)) {

                markReadNotificationPrev($request_surat_id);

                $reqSuratExisting = new RequestSuratModel();
                $reqSuratExisting = $reqSuratExisting->getById($request_surat_id);

                $message = "Surat No " . $reqSuratExisting->no_surat . " (" . $reqSuratExisting->judul_surat . ") telah ditolak, silahkan revisi ulang";
                insertNotification($request_surat_id, $reqSuratExisting->created_by, $message, getSessionUserId());

                $this->addPoint($request_surat_id, INCREASE_POINT);

                $res = array('success' => true, 'message' => ' Request Surat berhasil direject', 'redirect' => base_url('requestsurat/staff'));
            } else {
                $res = array('success' => false, 'message' => 'Request Surat gagal direject');
            }
        } else {
            $res = array('success' => false, 'message' => 'Minimal salah satu revisi harus disi');
        }


        printJson($this, $res);
    }

    public function doRevisi()
    {
        $request_surat_id = $this->input->post('request_surat_id');

        if ($request_surat_id) {
            $surat = new RequestSuratModel();
            $surat = $surat->getById($request_surat_id);

            $requestSuratDetailNo = new RequestSuratDetailModel();
            $lastRevNo = $requestSuratDetailNo->getLastRevNo($request_surat_id);
            $newRevisiNo = $lastRevNo + 1;

            if ($surat->revision_no == $lastRevNo) {
                $requestSuratDetail = new RequestSuratDetailModel();
                $requestSuratDetailList = $requestSuratDetail->getByRequestSuratId($request_surat_id);

                foreach ($requestSuratDetailList as $requestSuratDetail) {
                    $newRequestSuratDetail = new RequestSuratDetailModel();
                    $newRequestSuratDetail->request_surat_id = $requestSuratDetail->request_surat_id;
                    $newRequestSuratDetail->type_detail = $requestSuratDetail->type_detail;
                    $newRequestSuratDetail->isi = $requestSuratDetail->isi;
                    $newRequestSuratDetail->comment = $requestSuratDetail->comment;
                    $newRequestSuratDetail->revision_no = $newRevisiNo;
                    $newRequestSuratDetail->created_by = getSessionUserId();
                    $newRequestSuratDetail->created_date = now();
                    $newRequestSuratDetail->dbInsert($newRequestSuratDetail->fetch());
                }
            } else {
                $newRevisiNo = $lastRevNo;
            }

            $res = array('success' => true, 'message' => 'Request Surat berhasil direvisi', 'redirect' => base_url() . 'requestsurat/revisi/' . $request_surat_id . '/' . $newRevisiNo);
        } else {
            $res = array('success' => false, 'message' => '');
        }

        printJson($this, $res);
    }

    public function revisi($id = null)
    {
        if ($id) {
            $action = ($id == null) ? 'Add' : 'Edit';
            $data['cUri'] = base_url($this->cUri);
            $data['title'] = 'Detail Surat';

            $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
            $data['breadcrumb'] = $breadcrumb;

            $data['request_surat_id'] = $id;

            $requestsurat = new RequestSuratModel();

            if ($id == null) {
                $data['model'] = null;
            } else {
                $requestsurat = $requestsurat->getById($id);
                $data['requestSurat'] = $requestsurat;

                $jenisSuratExist = new JenisSuratModel();
                $data['jenisSurat'] = $jenisSuratExist->getById($requestsurat->jenis_surat_id);
            }
//
            $jenisurat = new JenisSuratModel();
            $data['jenissuratList'] = $jenisurat->dbGetAll('jenis_name');

            $requestSuratDetailNo = new RequestSuratDetailModel();
            $lastRevNo = $requestSuratDetailNo->getLastRevNo($id);

            $requestSuratDetail = new RequestSuratDetailModel();
            $data['requestSuratDetailList'] = $requestSuratDetail->getByRequestSuratId($id, $lastRevNo);

            $this->load->view('HeaderView', $data);
            $this->load->view('RevisiFormView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    function revisiIsi()
    {
        $request_surat_detail_id = $this->input->post('request_surat_detail_id');

        $requestSuratDetail = new RequestSuratDetailModel();
        $requestSuratDetail->isi = (trim($this->input->post('isi')));

        if ($requestSuratDetail->dbUpdate($requestSuratDetail->fetch(), $request_surat_detail_id)) {
            $res = array('success' => true, 'message' => 'Revisi berhasil ditambahakn', 'redirect' => base_url($this->cUri));
        } else {
            $res = array('success' => false, 'message' => 'Revisi gagal ditambahkan');
        }


        printJson($this, $res);
    }

    function doSubmitRevisi()
    {
        $request_surat_id = $this->input->post('request_surat_id');

        $requestSuratApprove = new RequestSuratModel();
        $requestSuratApprove->status = 'ASSIGNED';

        if ($requestSuratApprove->dbUpdate($requestSuratApprove->fetch(), $request_surat_id)) {
            $reqSuratExisting = new RequestSuratModel();
            $reqSuratExisting = $reqSuratExisting->getById($request_surat_id);

            $message = "Surat No " . $reqSuratExisting->no_surat . " (" . $reqSuratExisting->judul_surat . ") telah direvisi, mohon diverfikasi kembali";

            $requestSuratAssign = new RequestSuratAssignModel();
            $requestSuratAssign = $requestSuratAssign->getLastUser($request_surat_id);
            insertNotification($request_surat_id, $requestSuratAssign->user_id, $message, getSessionUserId());

            $userNotification = new UserModel();
            $userNotificationList = $userNotification->getUserByRole('ADMIN');
            foreach ($userNotificationList as $user) {
                insertNotification($request_surat_id, $user->user_id, $message, getSessionUserId());
            }

            $requestSuratDetailNo = new RequestSuratDetailModel();
            $lastRevNo = $requestSuratDetailNo->getLastRevNo($request_surat_id);

            $updateRev = new RequestSuratModel();
            $updateRev->revision_no = $lastRevNo;
            $updateRev->dbUpdate($updateRev->fetch(), $request_surat_id);

            $res = array('success' => true, 'message' => ' Request Surat berhasil direvisi', 'redirect' => base_url('requestsurat/requester'));
        } else {
            $res = array('success' => false, 'message' => 'Request Surat gagal direvisi');
        }

        printJson($this, $res);
    }

    public function getTotalUnReadSurat()
    {
        $surat = new RequestSuratModel();
        $totalUnReadSurat = $surat->dbGetRow("SELECT COUNT(*) AS total_unread FROM notification
                                            WHERE is_viewed = 0 and user_id = " . getSessionUserId());
        $totalUnReadSurat = $totalUnReadSurat->total_unread == 0 ? '' : $totalUnReadSurat->total_unread;
        $response = array('success' => true, 'totalUnread' => $totalUnReadSurat);
        printJson($this, $response);
    }

    public function getDataUnReadSurat()
    {
        $surat = new RequestSuratModel();
        $dataUnReadList = $surat->dbGetRows("SELECT notification.*, user.nama_lengkap FROM notification
                                            inner join user on notification.created_by = user.user_id
                                            WHERE is_viewed = 0 and notification.user_id = " . getSessionUserId() . " limit 5");


        $html = '<div class="drop-content" id="notification-dropdown-menu">';
        foreach ($dataUnReadList as $data) {
            $html .= '<li>
                        <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img">
                        <div class="img-circle" style="position: relative; width: 50px; height: 50px; background: linear-gradient(#0D9FB6, #073D6B);">
                                    <span class="vertical-center" style="color: white">' . strtoupper(substr($data->nama_lengkap, 0, 1)) . '</span>
                                    </div>
                        </div></div>
                            <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"> ' . $data->nama_lengkap . '
                                <p>' . $data->message . '</p>
                                <p class="time">' . $data->created_date . '</p>
                            </div>
                        </li>';

            /*$html .= '<li>' . $data->message . '</li>
                        <li style="font-size: 11px">' . $data->created_date . '</li>
                        <li role="separator" class="divider"></li>';*/
        }

        $html .= '</div>';

        echo $html;
    }

}