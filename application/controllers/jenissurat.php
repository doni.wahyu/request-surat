<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisSurat extends MY_Controller
{
    var $title = "Jenis Surat";
    var $cUri = "jenissurat";

    function __construct()
    {
        parent::__construct();
        $this->load->model('JenisSuratModel');
    }



    public function getAll()
    {
        $page = $this->input->get('page') == "" ? JQGRID_PAGE_START : $this->input->get('page');
        $limit = $this->input->get('limit') == "" ? JQGRID_PAGE_LIMIT : $this->input->get('limit');
        $order = $this->input->get('order') == "" ? '' : $this->input->get('order');
        $sort = $this->input->get('sort') == "" ? 'jenis_surat_id' : $this->input->get('sort');
        $searchField = $this->input->get('searchField');
        $searchValue = $this->input->get('searchValue');

        $jenisurat = new JenisSuratModel();
        $where = array();

        if ($searchField != '' && $searchValue != '') {
            array_push($where, array($searchField . " like" => "%" . $searchValue . "%"));
        }

        $count = $jenisurat->countAll($where);
        $totalPages = $count > 0 ? ceil($count / $limit) : 0;
        $data = $jenisurat->getAll($where, $sort, $order, $limit, $page);

        $res = array('data' => $data, 'totalPages' => $totalPages, 'recordCount' => $count, 'currPage' => $page, 'success' => true);
        printJson($this, $res);
    }


}